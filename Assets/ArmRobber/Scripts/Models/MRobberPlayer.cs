﻿using Rewired;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class MRobberPlayer 
{
    public Guid Id => _id;
    public string Name => _name;
    public Player RewiredPlayer => _rewiredPlayer;
    public RobotModel RobotModel => _robotModel;
    public bool IsLocal => _rewiredPlayer != null;
    
    [SerializeField]
    private string _name;
    [SerializeField]
    private Guid _id;
    [SerializeField]
    [NonSerialized]
    private Player _rewiredPlayer;
    [SerializeField]
    [NonSerialized]
    private RobotModel _robotModel;
    [SerializeField]
    private string _resourcesPath;

    public MRobberPlayer() { }

    public MRobberPlayer(string name, Guid id, Player rewiredPlayer)
    {
        _name = name;
        _rewiredPlayer = rewiredPlayer;
        _id = id;


        // TODO : per ora metterlo hardcodato qui, ma successivamente metterlo come parametro

        //string path = ModelContainer.Players.Count > 1 ? ResourcesUtilsPath.FLOWY_PATH : ResourcesUtilsPath.MR_PESCI_PATH;

        //RobotModel robotModel = Resources.Load<RobotModel>(path).CreateCopy();

        //RobotModel robotModel = Resources.Load<RobotModel>(ResourcesUtilsPath.COMMON_GENERIC_PATH).CreateCopy();

        string path = ModelContainer.Players.Count > 1 ? ResourcesUtilsPath.FLOWY_PATH : ResourcesUtilsPath.MR_PESCI_PATH;

        RobotModel robotModel = Resources.Load<RobotModel>(path).CreateCopy();

        robotModel.Init(this);
        _robotModel = robotModel;
    }

    public MRobberPlayer(string name,Guid guid, RobotModel robotModel)
    {
        _name = name;
        _id = guid;
        _robotModel = robotModel;
    }
    
    public static MRobberPlayer Copy(MRobberPlayer original)
    {
        var copy = new MRobberPlayer();

        copy._rewiredPlayer = original.RewiredPlayer;
        copy._robotModel= original.RobotModel.CreateCopy();
        copy._name = original.Name;
        copy._id = original._id;
        copy._resourcesPath = original._resourcesPath;

        return copy;
        
    }

    public static object Deserialize(byte[] data)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(data, 0, data.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        var model = (MRobberPlayer)binForm.Deserialize(memStream);

        RobotModel robotModel = Resources.Load<RobotModel>(ResourcesUtilsPath.COMMON_GENERIC_PATH).CreateCopy();
        robotModel.Init(model);
        model._robotModel = robotModel;

        return model;
    }

    public static byte[] Serialize(object customType)
    {
        var model = Copy((MRobberPlayer)customType);

        model._rewiredPlayer = null;

        BinaryFormatter bf = new BinaryFormatter();
        using (MemoryStream ms = new MemoryStream())
        {
            bf.Serialize(ms, model);
            return ms.ToArray();
        }
    }
}
