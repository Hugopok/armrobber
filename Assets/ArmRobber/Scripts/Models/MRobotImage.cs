﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class MRobotImage 
{
    public string ImageType;

    public Sprite SpriteImg;
}
