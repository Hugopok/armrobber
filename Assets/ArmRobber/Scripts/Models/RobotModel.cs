﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RobotModel", menuName = "ArmRobber/Models/Create RobotModel")]
[System.Serializable]
public class RobotModel : ScriptableObject
{
    #region Properties

    [Newtonsoft.Json.JsonIgnore]
    public MRobberPlayer ArmRobberPlayer => _armRobberPlayer;
    public string RobotName => _robotName;
    [Newtonsoft.Json.JsonIgnore]
    public GameObject Prefab => _prefab;
    public string RobotPath => _robotPath;
    #endregion

    [Header("Base")]
    [SerializeField]
    private string _robotName;
    [SerializeField]
    private string _robotPath;
    [Newtonsoft.Json.JsonIgnore]
    [System.NonSerialized]
    private MRobberPlayer _armRobberPlayer;
    [SerializeField]
    private GameObject _prefab;

    [Header("Images based by humor")]
    [Newtonsoft.Json.JsonIgnore]
    public List<MRobotImage> Images = new List<MRobotImage>();

    public void Init(MRobberPlayer armRobberPlayer)
    {
        _armRobberPlayer = armRobberPlayer;
    }

    public RobotModel CreateCopy()
    {
        var model = CreateInstance<RobotModel>();

        model._robotPath = RobotPath;
        model._prefab = Prefab;
        model._robotName = RobotName;

        foreach (var img in Images)
        {
            if (img == null) continue;

            model.Images.Add(img);
        }
        
        return model;
    }
}
