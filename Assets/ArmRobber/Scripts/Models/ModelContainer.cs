﻿using System;
using System.Collections.Generic;

public static class ModelContainer
{
    // TODO: Svuotare la classe dopo ogni sessione 

    public static List<MRobberPlayer> Players = new List<MRobberPlayer>();

    public static MRobberPlayer GetMainPlayer()
    {
        return Players[0];
    }

    public static bool IsMainPlayer(Guid guid)
    {
        return GetMainPlayer().Id == guid;
    }

    public static List<MRobberPlayer> GetAllLocalPlayers()
    {
        return Players.FindAll(p => p.IsLocal);
    }

    public static List<MRobberPlayer> GetAllRemotePlayers()
    {
        return Players.FindAll(p => !p.IsLocal);
    }

    public static MRobberPlayer GetPlayerById(Guid guid)
    {
        return Players.Find(p => p.Id == guid);
    }

    public static List<string> GetAllLocalPlayersId()
    {
        List<string> ids = new List<string>();

        foreach (var p in GetAllLocalPlayers())
        {
            if (p == null) continue;

            ids.Add(p.Id.ToString());
        }

        return ids;
    }

    public static List<MRobberPlayer> GetPlayersCopy()
    {
        List<MRobberPlayer> copyList = new List<MRobberPlayer>();

        foreach (var p in Players)
        {
            if (p == null) continue;
            copyList.Add(p);
        }
        return copyList;
    }

    public static bool AlreadyExist(Guid guid)
    {
        return Players.Exists(p => p.Id == guid);
    }
}
