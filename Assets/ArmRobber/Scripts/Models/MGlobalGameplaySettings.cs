﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GlobalGameplaySettings", menuName = "ArmRobber/Settings/MGlobalGameplaySettings")]
public class MGlobalGameplaySettings : ScriptableObject
{
    public List<WeaponsPerPlayer> MaxWeaponsInGame => _maxWeaponsInGame;
    public int StartWithWeapons => _startWithWeapons;

    [SerializeField] 
    private List<WeaponsPerPlayer> _maxWeaponsInGame = new List<WeaponsPerPlayer>();
    [SerializeField][Tooltip("Decidi con quante braccia i giocatori devono iniziare")]
    private int _startWithWeapons;
}

[System.Serializable]
public class WeaponsPerPlayer
{
    public int CurrentPlayers;
    public int MaxWeapons;
}

