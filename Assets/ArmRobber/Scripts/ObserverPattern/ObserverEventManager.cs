﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ObserverEventManager
{
    private static Dictionary<string, List<IObservable>> _observers = new Dictionary<string, List<IObservable>>();

    public static void AddObserver(string eventName, IObservable observer)
    {
        if (_observers.ContainsKey(eventName))
        {
            if (_observers[eventName].Contains(observer))
            {
                Debug.Log("Observer already added");
                return;
            }

            _observers[eventName].Add(observer);
        }
        else
        {
            _observers.Add(eventName, new List<IObservable> { observer });
        }
    }

    public static void RemoveObserver(string eventName, IObservable observer)
    {
        if (_observers.ContainsKey(eventName))
        {
            if (_observers[eventName].Contains(observer))
            {
                _observers[eventName].Remove(observer);
            }
        }
    }

    public static void Trigger<T>(string eventName, T objectToPass)
    {
        // Prevenire la modifica dell'enumeratore durante l'iterazione

        Dictionary<string, List<IObservable>> observersTemp = GetTemporaryObservers();

        foreach (var kv in observersTemp)
        {
            if (kv.Key != eventName) continue;

            foreach (var observer in kv.Value)
            {
                if (observer == null) continue;

                observer.OnEvent(eventName,objectToPass);
            }
        }

        observersTemp.Clear();
    }

    private static Dictionary<string, List<IObservable>> GetTemporaryObservers()
    {
        Dictionary<string, List<IObservable>> observersTemp = new Dictionary<string, List<IObservable>>();

        foreach (var kv in _observers)
        {
            observersTemp.Add(kv.Key, kv.Value);
        }

        return observersTemp;
    }
}
