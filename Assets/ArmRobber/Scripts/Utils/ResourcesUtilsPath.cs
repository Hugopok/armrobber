﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResourcesUtilsPath 
{
    // MODELS
    public const string ROBOT_MODEL_GENERIC_PATH = "Models/Robots/GenericRobot";
    public const string FLOWY_PATH = "Models/Robots/Flowy";
    public const string MR_PESCI_PATH = "Models/Robots/MrPesci";
    public const string COMMON_GENERIC_PATH = "Models/Robots/GenericRobot";

    // MANAGERS
    public const string PLAYER_MANAGER_PATH = "Managers/PlayerManager";
    public const string GAME_MANAGER_PATH = "Managers/GameManager";
    public const string INPUT_MANAGER_PATH = "Managers/InputManager";
    public const string REWIRED_INPUT_MANAGER = "Managers/Rewired Input Manager";
    public const string COMMON_ROBOT = "Managers/Rewired Input Manager";
}
