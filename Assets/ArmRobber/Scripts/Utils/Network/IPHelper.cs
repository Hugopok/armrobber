﻿using System.Net;
using System.Text.RegularExpressions;

public static class IPHelper
{
    public static string GetPublicIP()
    {
        string externalIP = string.Empty;
        externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
        externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(externalIP)[0].ToString();
        return externalIP;

    }
}