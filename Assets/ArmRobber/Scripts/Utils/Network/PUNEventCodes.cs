﻿public static class PUNEventCodes
{
    public const byte DAMAGE_EVENT = 1;
    public const byte REQUEST_SPAWN_PLAYER = 2;
    public const byte SPAWN_PLAYER_EVENT = 3;
    public const byte BACK_TO_MAIN_MENU = 4;
    public const byte RETRY = 5;

}

