﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoAdjustSmooth : MonoBehaviour
{
    [Sirenix.OdinInspector.ReadOnly]
    private Smooth.SmoothSyncPUN2 _smooth;

    [Range(0,0.04f)]
    [SerializeField]
    private float _avoidJitter;

    void Start()
    {
        if (_smooth == null)
            _smooth = GetComponent<Smooth.SmoothSyncPUN2>();

        _smooth.interpolationBackTime = (float)(1.0f / (float)ArmNetworkManager.Instance.SendRate) + _avoidJitter;
    }
}
