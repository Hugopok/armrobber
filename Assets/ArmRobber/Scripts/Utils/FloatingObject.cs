﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatingObject : MonoBehaviour
{
    public float addY;
    public float frequency;
    public bool stop;

    private void Awake()
    {
        if (stop) return;

        Move();
    }

    private void Move()
    {
        float h = transform.position.y + addY;

        transform.DOMoveY(h,frequency).SetLoops(-1,LoopType.Yoyo).SetEase(Ease.Linear);
    }
}
