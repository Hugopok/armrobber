﻿public static class ObserverEventNames 
{
    public const string DEATH_EVENT = "Death";
    public const string WIN_EVENT = "Win";
    public const string POWER_UP_SPEED_EVENT = "Speed Up";
    public const string WEAPON_GRABBED_EVENT = "WeaponGrab";
    public const string DAMAGABLE_EVENT = "DmgEvent";
    public const string SHOOTING_EVENT = "ShootingEvent";
    public const string SHOOTING_COMPONENT_INITIALIZED = "SComponentInit";
    public const string WEAPON_DESTROY = "WDestroy";
}
