﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TransformBoolDictionary : SerializableDictionary<Transform, bool> { }
