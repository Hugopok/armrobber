﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class PUNSerializerUtility
{
    public static T Deserialize<T>(byte[] param)
    {
        using (MemoryStream ms = new MemoryStream(param))
        {
            IFormatter br = new BinaryFormatter();
            return (T)br.Deserialize(ms);
        }
    }

    public static byte[] Serialize(object param)
    {
        byte[] encMsg = null;
        using (MemoryStream ms = new MemoryStream())
        {
            IFormatter br = new BinaryFormatter();
            br.Serialize(ms, param);
            encMsg = ms.ToArray();
        }

        return encMsg;
    }
}
