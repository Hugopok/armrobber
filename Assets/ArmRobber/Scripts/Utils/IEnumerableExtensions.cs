﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IEnumerableExtensions 
{
    public static bool IsInBounds(this ICollection array, int index)
    {
        return (index >= 0) && (index < array.Count);
    }
}
