﻿using UnityEngine.Events;

[System.Serializable]
public class DamagableEvent : UnityEvent<DamagableComponent> { }
