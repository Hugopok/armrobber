﻿using System;
using UnityEngine.Events;

[Serializable]
public class MRobberPlayerEvent : UnityEvent<MRobberPlayer> { }
