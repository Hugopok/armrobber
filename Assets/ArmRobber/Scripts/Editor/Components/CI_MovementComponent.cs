﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovementComponent))]
public class CI_MovementComponent : Editor
{
    private MovementComponent _mComponent;

    private bool _canSeeMore = false;

    private void OnValidate()
    {
        if (_mComponent == null)
            _mComponent = (MovementComponent)target;
    }


    public override void OnInspectorGUI()
    {
        if(GUILayout.Toggle(_canSeeMore,"Mostra di più"))
        {

        }

        base.OnInspectorGUI();
    }
}
