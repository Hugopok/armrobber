﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using WindowHelper;

public enum TuningWindowsState
{
    Robots,
    Weapons,
    Bullets,
    Miscellaneous
}

public class TuningWindows : WindowTemplate
{
    private TuningWindowSettings _windowSettings;
    private SerializedObject _currentObject;

    #region Tabs
    private TuningWindowsState _selectedTab = TuningWindowsState.Robots;
    private string[] _tabs = new string[] { "Robots", "Weapons","Bullets", "Miscellaneous" };
    #endregion

    #region Draw
    private Dictionary<string, UnityEngine.Object> _objectsLoaded = new Dictionary<string, UnityEngine.Object>();
    private Dictionary<string, bool> _foldouts = new Dictionary<string, bool>();
    private Vector2 _scroll = Vector2.zero;

    private bool _editComponent = false;
    private bool _editFeedbacks = false;
    #endregion

    [MenuItem("ArmRobber/Tuning Window")] // Attribute to create a little context menu 
    public static new TuningWindows OpenWindow()
    {
        var window = EditorWindow.GetWindow<TuningWindows>();

        window.OnOpenWindow<TuningWindowSettings>(typeof(TuningWindowSettings));

        return window;
    }

    public override void OnOpenWindow<T>(Type settingType)
    {
        base.OnOpenWindow<T>(settingType);

        this._windowSettings = (TuningWindowSettings)this.settings;

        if (this._windowSettings == null)
        {
            return;
        }
    }

    public override void OnGUi()
    {
        base.OnGUi();

        if (_windowSettings == null)
        {

            if (this.LoadPathManually<TuningWindowSettings>())
            {
                _windowSettings = (TuningWindowSettings)this.settings;
            }
        }
        else
            DrawWindow();
    }

    public override void OnProjectChanged()
    {
        base.OnProjectChanged();

        LoadObjects("t:prefab", new[] { "Assets/ArmRobber/Resources/PhotonPrefabs/Core/Robots" });
        LoadObjects("t:prefab", new[] { "Assets/ArmRobber/Resources/PhotonPrefabs/Core/WeaponsAndBullets" });
        LoadObjects("t:prefab", new[] { "Assets/ArmRobber/Resources/Managers" });
        LoadObjects("t:ScriptableObject", new[] { "Assets/ArmRobber/Resources/Models/Settings" });
    }

    private void OnFocus()
    {
        LoadObjects("t:prefab", new[] { "Assets/ArmRobber/Resources/PhotonPrefabs/Core/Robots" });
        LoadObjects("t:prefab", new[] { "Assets/ArmRobber/Resources/PhotonPrefabs/Core/WeaponsAndBullets" });
        LoadObjects("t:prefab", new[] { "Assets/ArmRobber/Resources/Managers" });
        LoadObjects("t:ScriptableObject", new[] { "Assets/ArmRobber/Resources/Models/Settings" });
    }

    private void DrawWindow()
    {
        GUILayout.BeginVertical(GUI.skin.box);

        _selectedTab = (TuningWindowsState)GUILayout.Toolbar((int)_selectedTab, _tabs);

        switch (_selectedTab)
        {
            case TuningWindowsState.Robots:
                DrawRobots();
                break;
            case TuningWindowsState.Weapons:
                DrawWeapons();
                break;
            case TuningWindowsState.Bullets:
                DrawBullets();
                break;
            case TuningWindowsState.Miscellaneous:
                DrawMiscellaneous();
                break;
            default:
                break;
        }

        GUILayout.EndVertical();
    }

    private void DrawRobots()
    {
        GUILayout.Space(10);

        _scroll = GUILayout.BeginScrollView(_scroll, false, false);

        foreach (var kv in _objectsLoaded)
        {
            GUILayout.Space(20);

            if (kv.Value == null) continue;

            var go = kv.Value as GameObject;

            if (go == null) continue;

            var pComponent = go.GetComponent<PlayerComponent>();

            if (pComponent == null) continue;

            #region Foldout

            bool foldoutValue = GetFoldoutValue(go.name);

            foldoutValue = EditorGUILayout.Foldout(foldoutValue, go.name, true);

            SetFoldoutValue(go.name, foldoutValue);

            if (!foldoutValue) continue;

            #endregion

            GUILayout.BeginVertical(GUI.skin.box);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button(AssetPreview.GetAssetPreview(go), GUILayout.Width(80), GUILayout.Height(80)))
            {
                Selection.activeObject = go;
                EditorGUIUtility.PingObject(go);
            }

            if (GUILayout.Button("Edita componenti"))
            {
                _editComponent = true;
                _editFeedbacks = false;
            }

            if (GUILayout.Button("Edita feedbacks"))
            {
                _editFeedbacks = true;
                _editComponent = false;
            }

            GUILayout.EndHorizontal();

            if (_editComponent)
            {
                DrawProperties<DamagableComponent>(go.GetComponents<DamagableComponent>());
                DrawProperties<BehaviourBase>(go.GetComponents<BehaviourBase>().ToList().FindAll(item => item.GetType() != typeof(PauseComponent)).ToArray());
            }
            else if (_editFeedbacks)
            {
                DrawProperties<AFeedback>(go.transform.Find("Feedbacks")?.GetComponents<AFeedback>());
            }
            GUILayout.EndVertical();
        }

        GUILayout.EndScrollView();
    }

    private void DrawBullets()
    {
        _scroll = GUILayout.BeginScrollView(_scroll, false, false);

        foreach (var kv in _objectsLoaded)
        {
            GUILayout.Space(20);

            if (kv.Value == null) continue;

            var go = kv.Value as GameObject;

            if (go == null) continue;

            var bullet = go.GetComponent<Bullet>();

            if (bullet == null) continue;

            #region Foldout

            bool foldoutValue = GetFoldoutValue(go.name);

            foldoutValue = EditorGUILayout.Foldout(foldoutValue, go.name, true);

            SetFoldoutValue(go.name, foldoutValue);

            if (!foldoutValue) continue;

            #endregion

            GUILayout.BeginVertical(GUI.skin.box);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button(AssetPreview.GetAssetPreview(go), GUILayout.Width(80), GUILayout.Height(80)))
            {
                Selection.activeObject = go;
                EditorGUIUtility.PingObject(go);
            }

            GUILayout.EndHorizontal();
            
            DrawProperties<Bullet>(go.GetComponents<Bullet>());
            
            GUILayout.EndVertical();
        }

        GUILayout.EndScrollView();
    }

    private void DrawWeapons()
    {
        GUILayout.Space(10);

        _scroll = GUILayout.BeginScrollView(_scroll, false, false);

        foreach (var kv in _objectsLoaded)
        {
            GUILayout.Space(20);

            if (kv.Value == null) continue;

            var go = kv.Value as GameObject;

            if (go == null) continue;

            var armWeapon = go.GetComponent<ArmWeapon>();
            
            if (armWeapon == null) continue;

            #region Foldout

            bool foldoutValue = GetFoldoutValue(go.name);

            foldoutValue = EditorGUILayout.Foldout(foldoutValue, go.name, true);

            SetFoldoutValue(go.name, foldoutValue);

            if (!foldoutValue) continue;

            #endregion

            GUILayout.BeginVertical(GUI.skin.box);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button(AssetPreview.GetAssetPreview(go), GUILayout.Width(80), GUILayout.Height(80)))
            {
                Selection.activeObject = go;
                EditorGUIUtility.PingObject(go);
            }

            if (GUILayout.Button("Edita componenti"))
            {
                _editComponent = true;
                _editFeedbacks = false;
            }

            if (GUILayout.Button("Edita feedbacks"))
            {
                _editFeedbacks = true;
                _editComponent = false;
            }

            GUILayout.EndHorizontal();

            if (_editComponent)
            {
                
                DrawProperties<ArmWeapon>(go.GetComponents<ArmWeapon>());
                DrawProperties<Pickable>(go.GetComponents<Pickable>());
            }
            else if (_editFeedbacks)
            {
                DrawProperties(go.transform.Find("Feedbacks")?.GetComponents<AFeedback>());
            }
            GUILayout.EndVertical();
        }

        GUILayout.EndScrollView();
    }

    private void DrawMiscellaneous()
    {
        GUILayout.Space(10);

        SettingsManager settings = null;
        EnvironmentManager environmentManager = null;

        foreach (GameObject item in _objectsLoaded.Values.ToList())
        {
            if (settings != null && environmentManager != null) break;

            if (item == null) continue;

            if(settings == null && item.GetComponent<SettingsManager>() != null)
            {
                settings = item.GetComponent<SettingsManager>();
                continue;
            }

            if (environmentManager == null && item.GetComponent<EnvironmentManager>() != null)
            {
                environmentManager = item.GetComponent<EnvironmentManager>();
                continue;
            }
        }

        _scroll = GUILayout.BeginScrollView(_scroll, false, false);

        if(settings != null)
            DrawProperties<MGlobalGameplaySettings>(new MGlobalGameplaySettings[] { settings.GameplaySettings });
        if (environmentManager != null)
            DrawProperties<EnvironmentManager>(new EnvironmentManager[] { environmentManager });

        GUILayout.EndScrollView();
    }

    private void DrawProperties<T>(T[] objects) where T : UnityEngine.Object
    {
        int i = 0;

        Color originalBG = GUI.backgroundColor;
        Color nextColor = Color.cyan;

        foreach (var behaviour in objects)
        {
            GUILayout.Space(25);

            #region Foldout

            if (i % 2 != 0)
            {
                GUI.backgroundColor = originalBG;
            }
            else
                GUI.backgroundColor = nextColor;

            GUILayout.BeginHorizontal(GUI.skin.box);

            string key = behaviour.name + behaviour.GetInstanceID();

            var folodoutValue = GetFoldoutValue(key);

            i++;

            folodoutValue = EditorGUILayout.Foldout(folodoutValue, behaviour.GetType().Name, true);

            SetFoldoutValue(key, folodoutValue);

            if(GUILayout.Button("Remove"))
            {
                DestroyImmediate(behaviour,true);
                _currentObject.ApplyModifiedProperties();
                _currentObject.Update();
                break;
            }

            GUILayout.EndHorizontal();

            #endregion

            if (!folodoutValue) continue;

            GUI.backgroundColor = originalBG;

            _currentObject = new SerializedObject(behaviour);

            var properties = GetProperties(_currentObject);

            foreach (var item in properties)
            {
                EditorGUILayout.PropertyField(item, true);
            }

            _currentObject.ApplyModifiedProperties();
            _currentObject.Update();
        }

        GUI.backgroundColor = originalBG;

    }

    private IEnumerable<SerializedProperty> GetProperties(SerializedObject obj)
    {
        var iterator = obj.GetIterator();
        var enterChildren = true;
        while (iterator.NextVisible(enterChildren))
        {
            var type = obj.targetObject.GetType();
            var fields = type.GetFields(BindingFlags.Instance |
                                    BindingFlags.NonPublic |
                                    BindingFlags.Public);

            bool skip = false;

            foreach (var p in fields)
            {
                if (p.Name != iterator.name) continue;

                foreach (var att in p.CustomAttributes)
                {
                    skip = (att.AttributeType == typeof(HideInWindow));

                    if (skip) break;
                }
                if (skip) break;
            }

            if (skip) continue;

            enterChildren = false;
            yield return iterator;
        }
    }

    private void LoadObjects(string type, string[] folders)
    {
        foreach (var guid in AssetDatabase.FindAssets(type, folders))
        {
            //string assetPath = AssetDatabase.GetAssetPath(item);

            if (_objectsLoaded.ContainsKey(guid)) continue;

            _objectsLoaded.Add(guid, AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(AssetDatabase.GUIDToAssetPath(guid)));
        }
    }

    private void LoadObjects(string type)
    {
        foreach (var guid in AssetDatabase.FindAssets(type))
        {
            //string assetPath = AssetDatabase.GetAssetPath(item);

            if (_objectsLoaded.ContainsKey(guid)) continue;

            _objectsLoaded.Add(guid, AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(AssetDatabase.GUIDToAssetPath(guid)));
        }
    }

    private bool GetFoldoutValue(string key)
    {
        if (_foldouts.ContainsKey(key))
            return _foldouts[key];

        _foldouts.Add(key, false);

        return false;
    }

    private void SetFoldoutValue(string key, bool value)
    {
        if (_foldouts.ContainsKey(key))
        {
            if (_foldouts[key] == value) return;

            _foldouts[key] = value;
            return;
        }

        _foldouts.Add(key, value);
    }

    protected override string GetSettingsPath()
    {
        return string.Empty;
    }
}
