﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowHelper;

[CreateAssetMenu(fileName = "TuningWindowSettings", menuName = "ArmRobber/WindowHelper/Create new tuning window settings")]
public class TuningWindowSettings : WindowSettings
{

}
