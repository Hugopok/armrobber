﻿using System.Collections;
using System.IO;
using UnityEngine;

public static class EditorPathsUtils
{
    public const string UI_MANAGER_PATH = "Assets/ArmRobber/Resources/Managers/UIManager.prefab";
    public const string PLAYER_MANAGER_PATH = "Assets/ArmRobber/Resources/Managers/PlayerManager.prefab";
    public const string GAME_MANAGER_PATH = "Assets/ArmRobber/Resources/Managers/GameManager.prefab";
    public const string INPUT_MANAGER_PATH = "Assets/ArmRobber/Resources/Managers/InputManager.prefab";
    public const string NETWORK_MANAGER_PATH = "Assets/ArmRobber/Resources/Managers/ArmNetworkManager.prefab";
    public const string REWIRED_INPUT_MANAGER = "Assets/ArmRobber/Resources/Managers/Rewired Input Manager.prefab";

    public static T[] GetAtPath<T>(string path) where T : UnityEngine.Object
    {

        ArrayList al = new ArrayList();
        string[] fileEntries = Directory.GetFiles(Application.dataPath + "/" + path);
        foreach (string fileName in fileEntries)
        {
            int index = fileName.LastIndexOf("/");
            string localPath = "Assets/" + path;

            if (index > 0)
                localPath += fileName.Substring(index);

            Object t = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(localPath);

            if (t != null)
                al.Add(t);
        }
        T[] result = new T[al.Count];
        for (int i = 0; i < al.Count; i++)
            result[i] = (T)al[i];

        return result;
    }

}