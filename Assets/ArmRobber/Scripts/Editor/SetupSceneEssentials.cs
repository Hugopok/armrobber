﻿using UnityEditor;
using UnityEngine;

public static class SetupSceneEssentials
{
    private static UIManager uiManager = GameObject.FindObjectOfType<UIManager>();
    private static PlayerManager pManager = GameObject.FindObjectOfType<PlayerManager>();
    private static GameManager gameManager = GameObject.FindObjectOfType<GameManager>();
    private static InputManager inputManager = GameObject.FindObjectOfType<InputManager>();
    private static Rewired.InputManager rewiredInputManager = GameObject.FindObjectOfType<Rewired.InputManager>();
    private static ArmNetworkManager networkManager = GameObject.FindObjectOfType<ArmNetworkManager>();

    [MenuItem("ArmRobber/CreateManagersAsPrefabs")]
    [MenuItem("GameObject/ArmRobber/Create Managers As Prefabs", false, 1)]
    public static void CreateManagersAsPrefabs()
    {
        if (inputManager == null)
        {
            var obj = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(EditorPathsUtils.INPUT_MANAGER_PATH), null);

            if (obj != null) inputManager = ((GameObject)obj).GetComponent<InputManager>();

            var objRewired = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(EditorPathsUtils.REWIRED_INPUT_MANAGER), null);

            if (objRewired != null) rewiredInputManager = ((GameObject)obj).GetComponent<Rewired.InputManager>();
        }

        if (gameManager == null)
        {
            var obj = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(EditorPathsUtils.GAME_MANAGER_PATH), null);

            if (obj != null)
            {
                gameManager = ((GameObject)obj).GetComponent<GameManager>();
                gameManager.Debug = true;
            }
        }

        if (pManager == null)
        {
            var obj = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(EditorPathsUtils.PLAYER_MANAGER_PATH), null);

            if (obj != null) pManager = ((GameObject)obj).GetComponent<PlayerManager>();
        }

        if(uiManager == null)
        {
            var obj = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(EditorPathsUtils.UI_MANAGER_PATH), null);

            if (obj != null) uiManager = ((GameObject)obj).GetComponent<UIManager>();
        }

        if(networkManager == null)
        {
            var obj = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath<GameObject>(EditorPathsUtils.NETWORK_MANAGER_PATH), null);

            if (obj != null) networkManager = ((GameObject)obj).GetComponent<ArmNetworkManager>();
        }
    }

    [MenuItem("ArmRobber/Enable Debug")]
    public static void EnableDebug()
    {
        if (gameManager != null)
        {
            gameManager.Debug = true;
            EditorUtility.SetDirty(gameManager);
        }
    }

    [MenuItem("ArmRobber/Disable Debug")]
    public static void DisableDebug()
    {
        if (gameManager != null)
        {
            gameManager.Debug = false;

            EditorUtility.SetDirty(gameManager);
        }
    }

    [MenuItem("ArmRobber/Destroy Managers (IMPORTANTISSIMO! distruggere i prefab se ci si trova in una scena che non è main menu)")]
    public static void DestroyManagers()
    {
        if (gameManager != null) GameObject.DestroyImmediate(gameManager.gameObject);

        if (inputManager != null) GameObject.DestroyImmediate(inputManager.gameObject);

        if (pManager != null) GameObject.DestroyImmediate(pManager.gameObject);

        if (rewiredInputManager != null) GameObject.DestroyImmediate(rewiredInputManager.gameObject);

        if (networkManager != null) GameObject.DestroyImmediate(networkManager.gameObject);
    }
}
