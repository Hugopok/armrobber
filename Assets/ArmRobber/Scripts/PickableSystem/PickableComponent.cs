﻿using Photon.Pun;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

public class PickableComponent : BehaviourBase
{
    [SerializeField] private float _castRadius;

    [ReadOnly][SerializeField][HideInWindow]
    private DamagableComponent damagableComponent;
    [ReadOnly][SerializeField][HideInWindow]
    private Collider[] colls = new Collider[] { };

    private bool cast;

    protected override void Awake()
    {
        base.Awake();

#if !UNITY_EDITOR

        if (!PhotonNetwork.IsMasterClient) return;
#else
        if (!GameManager.Instance.Debug && !PhotonNetwork.IsMasterClient) return; 
#endif

        Debug.Log("Pickable component initialization",gameObject);
        
        cast = true;

        damagableComponent = GetComponent<DamagableComponent>();

        if (damagableComponent == null) return;

        damagableComponent.OnDamage.AddListener(Damage);

        damagableComponent.OnDeath.AddListener(DisableOnDeath);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        StopAllCoroutines();

        damagableComponent?.OnDeath.RemoveListener(DisableOnDeath);
        damagableComponent?.OnDamage.RemoveListener(Damage);
    }

    private void Damage(DamagableComponent damagableComponent)
    {
        StartCoroutine(DisablePick(damagableComponent.InvulnerableTime));
    }

    private IEnumerator DisablePick(float duration)
    {
        cast = false;

        yield return new WaitForSeconds(duration);

        cast = true;
    }

    private void DisableOnDeath(DamagableComponent damagable)
    {
        cast = false;
    }

    protected virtual void CustomCollider()
    {
        if (this == null || gameObject == null)
            GameManager.Instance.OnFixedUpdate -= FixedUpdateLoop;

        //Debug.Log("Pickable component running on " + gameObject.name);

        if (!cast) return;

        colls = Physics.OverlapSphere(transform.position, _castRadius);

        if (colls.Length == 0) return;

        foreach (var item in colls)
        {
            var pickable = item.gameObject.GetComponent<Pickable>();

            if (pickable == null) continue;
            if (pickable.IsPicked()) continue;

            pickable.Pick(gameObject);
        }
    }

    protected override void FixedUpdateLoop()
    {
        CustomCollider();
    }

    protected override void PauseUpdateLoop() { }

    protected override void UpdateLoop() { }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, _castRadius);
    }
#endif
}
