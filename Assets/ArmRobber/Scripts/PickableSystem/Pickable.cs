﻿using System;
using System.Collections;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    [SerializeField] protected GameObject _notPickedParticle;
    [SerializeField] protected GameObject _pickedParticle;

    protected ParticleSystem _pickedParticleS;
    protected ParticleSystem _notPickedParticleS;

    protected virtual void Awake()
    {
        if (_pickedParticle != null)
            _pickedParticleS = _pickedParticle.GetComponent<ParticleSystem>();

        if (_notPickedParticle)
            _notPickedParticleS = _notPickedParticle.GetComponent<ParticleSystem>();
    }

    protected virtual void Update()
    {
        if (_notPickedParticle == null) return;

        if (!IsPicked() && !_notPickedParticle.activeInHierarchy)
        {
            NotPickedEffect();
        }
    }

    public virtual bool IsPicked()
    {
        return transform.parent != null;
    }

    public virtual void Pick(GameObject whoTake)
    {
        PickedEffect();
    }

    public virtual void PickedEffect()
    {
        if (_notPickedParticle != null)
            _notPickedParticle.SetActive(false);
        if (_pickedParticle != null)
            _pickedParticle?.SetActive(true);

        if (_pickedParticleS == null) return;

        StartCoroutine(WaitUntilFinished(_pickedParticleS, () =>
        {
            _pickedParticle.SetActive(false);
        }));
    }

    public virtual void NotPickedEffect()
    {
        _notPickedParticle?.SetActive(true);
        _pickedParticle?.SetActive(false);
    }

    protected virtual IEnumerator WaitUntilFinished(ParticleSystem particleToWait, Action onComplete)
    {
        yield return new WaitForSeconds(particleToWait.main.duration - 0.15f);

        onComplete?.Invoke();
    }
}
