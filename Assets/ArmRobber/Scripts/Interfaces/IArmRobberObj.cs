﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IArmRobberObj<T> where T : Component
{
    void Initialize(RobotModel model);
    T GetObject { get; set; }
}
