﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Pickable
{
    public bool IsActive => isActive;

    [SerializeField]
    protected float powerUpDuration;
    [SerializeField]
    protected bool disableAfterPick = true;
    [SerializeField]
    protected bool destroyAfterPick = false;

    protected bool isActive = false;

    public override bool IsPicked()
    {
        return isActive;
    }

    public override void Pick(GameObject whoTake)
    {
        isActive = true;

        if (destroyAfterPick)
        {
            gameObject.SetActive(false);
            Destroy(gameObject,0.5f);
            return;
        }

        if (disableAfterPick)
            gameObject.SetActive(false);
    }
}
