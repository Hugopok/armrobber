﻿using System;
using UnityEngine;

public class ArmWeaponPickable : Pickable
{
    public override void Pick(GameObject whoTake)
    {
        ObserverEventManager.Trigger<PowerUpEventData<Tuple<GameObject, GameObject>>>(ObserverEventNames.WEAPON_GRABBED_EVENT,
                                                                                    new PowerUpEventData<Tuple<GameObject, GameObject>>(0,
                                                                                    new Tuple<GameObject, GameObject>(gameObject, whoTake)));
    }
}
