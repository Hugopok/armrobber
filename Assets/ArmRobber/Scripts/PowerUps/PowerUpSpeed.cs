﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PowerUpSpeed : PowerUp
{
    [SerializeField] private float _speed;
    
    public override void Pick(GameObject whoTake)
    {
        ObserverEventManager.Trigger<PowerUpEventData<Tuple<float,GameObject>>>(ObserverEventNames.POWER_UP_SPEED_EVENT,
                             new PowerUpEventData<Tuple<float, GameObject>>(powerUpDuration, new Tuple<float, GameObject>(_speed, whoTake)));

        base.Pick(whoTake);
    }
}
