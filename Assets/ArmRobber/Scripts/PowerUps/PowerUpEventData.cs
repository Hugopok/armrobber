﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpEventData<T> 
{
    public readonly float TimeDuration;
    public readonly T Data;

    public PowerUpEventData(float duration,T data)
    {
        TimeDuration = duration;
        Data = data;
    }
}
