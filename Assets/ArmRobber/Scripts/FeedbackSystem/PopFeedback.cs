﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopFeedback : AFeedback
{
    [Space(10)]
    [Header("Trail effect settings")]
    [SerializeField] private GameObject _trailEffect;
    [SerializeField] public float TrailEffectDuration;
    private ParticleSystem _trailPs;

    [Space(10)]
    [Header("Landing effect settings")]
    [SerializeField] private GameObject _landingEffect;
    [SerializeField] private float _landingEffectDuration;
    private ParticleSystem _landingPs;

    public bool BruteForceStop = false;

    private void Start()
    {
        _trailEffect.SetActive(false);
        _landingEffect.SetActive(false);
        _landingPs = _landingEffect.GetComponent<ParticleSystem>();
        _trailPs = _trailEffect.GetComponent<ParticleSystem>();

        if (_landingEffectDuration <= 0)
            _landingEffectDuration = _landingPs.main.duration + 0.5f;
    }

    public override void Play(GameObject owner, object objectToPass)
    {
        bool isLanding = (bool)objectToPass;

        if (!isLanding)
            _trailPs.Play(true);
        else
            _landingPs.Play(true);

        playCoroutine =  isLanding ? 
                         StartCoroutine(PlayEffect(_landingEffectDuration,_landingEffect)) : 
                         StartCoroutine(PlayEffect(TrailEffectDuration,_trailEffect));
    }
       
    public override IEnumerator PlayCoroutine()
    {
        yield return null;
    }

    private IEnumerator PlayEffect(float duration, GameObject goToPlay) 
    {
        goToPlay.SetActive(true);

        if(duration > 0)
            yield return new WaitForSeconds(duration);
        else
        {
            while (!BruteForceStop)
            {
                yield return null;
            }

            BruteForceStop = false;
        }

        goToPlay.SetActive(false);

        if(goToPlay == _landingEffect)
            _landingPs.Stop(true,ParticleSystemStopBehavior.StopEmitting);
        else 
            _trailPs.Stop(true,ParticleSystemStopBehavior.StopEmitting);
    }

}
