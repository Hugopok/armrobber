﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NameUIFeedback : AFeedback
{
    [SerializeField]
    private TextMeshProUGUI _txt;
    [SerializeField]
    private PlayerComponent _playerComponent;
    [SerializeField]
    private GameObject _canvas;

    private void Start()
    {
        if (SManager.Instance.Scene == SceneName.MainMenu) return;

        _canvas.SetActive(true);

        _txt.SetText(_playerComponent.PlayerModel.Name);
    }

    public override void Play(GameObject owner, object objectToPass)
    {

    }

    public override IEnumerator PlayCoroutine()
    {
        yield break;
    }
}
