﻿using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;

public class ShootFeedback : AFeedback
{
    public Gradient Gradient => _gradient;
    public float EvaluateIndex { get => evaluateIndex; set => evaluateIndex = value; }

    [Header("Colors")]

    [SerializeField]
    private Gradient _gradient;

    [SerializeField]
    private string _colorProperty = "_BaseColor";

    [SerializeField]
    private float _stepToRemove = 0.25f;

    [Space(10)]
    [Header("Arm Animation")]

    [SerializeField]
    private float _duration;
    [SerializeField]
    private float _zPos;
    [SerializeField]
    private GameObject _goToAnimate;

    [Space(10)]
    [Header("DEBUG")]

    [SerializeField]
    private MeshRenderer _mr;

    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    private float evaluateIndex = 0;

    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    private int maxStep = 0;

    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    private float originalPos = 0;

    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    private bool _isAnimating;

    private void Start()
    {
        if (_mr == null)
            _mr = GetComponent<MeshRenderer>();

        if (_goToAnimate == null) return;

        originalPos = _goToAnimate.transform.localPosition.z;
    }

    public override void Play(GameObject owner, object objectToPass)
    {
        ArmWeapon weapon = (ArmWeapon)objectToPass;

        if (_mr != null)
        {
            Color startColor = _mr.material.GetColor(_colorProperty);

            LerpWeaponColor(Mathf.Abs(weapon.CurrentAmmo / weapon.MaxAmmos));
        }

        maxStep = weapon.MaxAmmoPerMag;

        AnimateArmOnShot(1 / weapon.FireRate);

    }

    public override IEnumerator PlayCoroutine()
    {
        yield break;
    }

    private void AnimateArmOnShot(float r)
    {
        if (_isAnimating) return;

        _isAnimating = true;

        if (_goToAnimate == null) return;

        _goToAnimate.transform.DOLocalMoveZ(_zPos, _duration).OnComplete(() =>
        {
            _goToAnimate.transform.DOLocalMoveZ(0, _duration / 2).SetEase(Ease.OutQuad).OnComplete(() =>
            {
                _isAnimating = false;
            });

        }).SetEase(Ease.OutQuad);
    }

    private void LerpWeaponColor(float step)
    {
        evaluateIndex += _stepToRemove;

        _mr?.material.SetColor(_colorProperty, _gradient.Evaluate(Mathf.Abs(evaluateIndex)));
    }
}
