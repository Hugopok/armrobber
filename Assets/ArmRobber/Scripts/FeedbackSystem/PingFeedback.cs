﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PingFeedback : MonoBehaviour, Photon.Pun.IPunObservable
{
    [SerializeField]
    private TextMeshProUGUI _txt;

    [SerializeField]
    PhotonView _photonView;

    [Sirenix.OdinInspector.ReadOnly]
    [SerializeField]
    private int _ping;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (_photonView.IsMine && stream.IsWriting)
        {
            stream.SendNext(_ping);
        }
        else if(!_photonView.IsMine && stream.IsReading)
        {
            _ping = (int)stream.ReceiveNext();
        }
    }

    private void Update()
    {
        if (_txt == null) return;
        
        if(_photonView.IsMine)
            _ping = PhotonNetwork.GetPing();

        _txt.SetText($"Ping {_ping}");
    }
}
