﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadFeedback : AFeedback
{
    [SerializeField]
    private ShootFeedback shootFeedback;

    [SerializeField]
    private string _colorProperty = "_BaseColor";

    [SerializeField]
    private MeshRenderer _mr;

    [SerializeField][Sirenix.OdinInspector.ReadOnly]
    private Color _originalColor;

    private void Start()
    {
        if (shootFeedback == null)
            shootFeedback = GetComponent<ShootFeedback>();

        if (_mr == null)
            _mr = GetComponent<MeshRenderer>();

        if (_mr != null)
            _originalColor = _mr.material.GetColor(_colorProperty);
    }

    public override void Play(GameObject owner, object objectToPass)
    {
        if (_mr != null)
            _mr.material.SetColor(_colorProperty, _originalColor);

        shootFeedback.EvaluateIndex = 0;
    }

    public override IEnumerator PlayCoroutine()
    {
        yield break;
    }
}
