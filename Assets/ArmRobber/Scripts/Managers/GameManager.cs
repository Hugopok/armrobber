﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Rewired;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// E' il manager principale, qui è contenuto il sistema ad eventi su cui gli oggetti si possono agganciare
/// per gestire meglio lo stato di pausa e di gioco, inoltre crea l'InputManagere il PlayerManager
/// se non ci sono in scena, e salva le loro istanze in memoria.
/// GameManager,InputManager e PlayerManager devono essere unici e persistere per tutta la durata dell'applicazione 
/// </summary>
public class GameManager : PunCallbackSingleton<GameManager> , IOnEventCallback
{
    #region Properties

    public bool IsOffline { get; private set; }
    public bool IsLoading { get; private set; }
    public bool IsSaving { get; private set; }
    public bool IsPaused => State == GameState.Pause;
    
    [Sirenix.OdinInspector.ShowInInspector]
    [Sirenix.OdinInspector.ReadOnly]
    public GameState State
    {
        get
        {
            return _state;
        }
        set
        {
            bool changed = _state != value;

            _state = value;

            if (_state == GameState.InGame)
                OnGameReady?.Invoke();

            if (changed)
                OnStateChanged?.Invoke(_state);
        }
    }

    #endregion

    #region Inspector_Fields

    public UnityEvent OnGameReady = new UnityEvent();

#if UNITY_EDITOR // Solo se si è in Editor altrimenti questi campi non esistono
    [Header("DEBUG")]
    [Sirenix.OdinInspector.InfoBox("Debug mode to test the game, remember to enable the Debug field")]
    public bool Debug;

    [Sirenix.OdinInspector.InfoBox("Start with state : Is active only in editor mode and when debug mode is enabled")]
    [SerializeField]
    [Sirenix.OdinInspector.EnableIf("Debug", true)]
    private GameState _startWithState;
#endif

    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    private static InputManager _inputManager;
    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    private static PlayerManager _playerManager;

    #endregion

    #region Non_Inspector_Fields_Or_Private

    public Action<GameState> OnStateChanged;
    public Action OnFixedUpdate;
    public Action OnUpdate;
    public Action OnPauseUpdate;

    private GameState _state;

    #endregion

    private void Awake()
    {
        if (IsInstanced() && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        CheckManagers();

        SManager.Instance.OnSceneLoaded += SceneChanged;

        _instance = this;

#if UNITY_EDITOR
        if (Debug)
        {
            PhotonNetwork.OfflineMode = true;
            PhotonNetwork.CreateRoom("");

            State = _startWithState;
        }

        QualitySettings.vSyncCount = 60;
#endif
        
    }

#if UNITY_EDITOR
    IEnumerator WaitOffline()
    {
        yield return new WaitForSeconds(5);

        LobbyManager.Instance.OfflineMode(true);
    }
#endif

    private void Update()
    {
        /* IMPORTANTE : Sistema di eventi
         * Ogni Behaviour base (o chi ne ha bisogno) deve agganciarsi al pauseUpdate o all' onupdate (anche fixedupdate)
         * cosi si può gestire molto facilmente lo stato di pausa o lo stato di gioco.
        */

        if (State == GameState.LoadingMap)
        {
            return;
        }

        // se state è uguale a uno di questi stati allora deve girare il pauseUpdate
        if (State == GameState.MainMenu ||
           State == GameState.Pause ||
           State == GameState.VictoryPause)
        {
            OnPauseUpdate?.Invoke();

            return;
        }
        // altrimenti fai girare l'update normale
        else if (State == GameState.InGame)
        {
            OnUpdate?.Invoke();
        }
    }

    private void FixedUpdate()
    {
        if (State == GameState.InGame && !IsPaused)
            OnFixedUpdate?.Invoke();
    }
    
    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (SManager.IsInstanced())
            SManager.Instance.OnSceneLoaded -= SceneChanged;

        OnGameReady.RemoveAllListeners();
        OnUpdate = null;
        OnPauseUpdate = null;
        OnFixedUpdate = null;

        _playerManager = null;
        _inputManager = null;

        ModelContainer.Players.Clear();
    }

    public void CheckManagers()
    {
        /* Se non ci sono l'inputmanager e/o il player manager allora li crea il game manager 
         * e si tiene l'istanza in memoria
         */

        _inputManager = FindObjectOfType<InputManager>();
        _playerManager = FindObjectOfType<PlayerManager>();

        if (_playerManager == null)
        {
            _playerManager = Instantiate(Resources.Load<GameObject>(ResourcesUtilsPath.PLAYER_MANAGER_PATH)).
                                        GetComponent<PlayerManager>();
        }

        if (_inputManager == null)
        {
            _inputManager = Instantiate(Resources.Load<GameObject>(ResourcesUtilsPath.INPUT_MANAGER_PATH)).
                                        GetComponent<InputManager>();
        }
    }

    public IEnumerator Retry()
    {
        State = GameState.Retry;

        var raiseOpt = RaiseEventOptions.Default;

        raiseOpt.Receivers = ReceiverGroup.All;

        PhotonNetwork.RaiseEvent(PUNEventCodes.RETRY, null, raiseOpt, SendOptions.SendReliable);

        yield return null;
    }

    public void MainMenu()
    {
        ResetNetwork();

        StartCoroutine(SManager.Instance.ChangeSceneTo(SceneName.MainMenu, null));
    }

    private void ResetNetwork()
    {
        foreach (var item in FindObjectsOfType<BehaviourBase>())
        {
            if (item == null || !item.PhotonView.IsMine) continue;

            PhotonNetwork.Destroy(item.gameObject);
        }

        if (_playerManager != null)
            _playerManager.Clear();

        OnUpdate = null;
        OnFixedUpdate = null;
        OnPauseUpdate = null;
    }

    private void SceneChanged(SceneName sceneName)// SceneName
    {
        switch (sceneName)
        {
            case SceneName.MainMenu:
                State = GameState.MainMenu;
                break;
            case SceneName.Test_Environment:
                PhotonNetwork.IsMessageQueueRunning = true;
                State = GameState.InGame;
                State = GameState.Pause;
                break;
            default:
                break;
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code != PUNEventCodes.RETRY) return;

        UnityEngine.Debug.Log("RPC_RETRY CALLED");

        PhotonNetwork.IsMessageQueueRunning = false;

        ResetNetwork();

        StartCoroutine(SManager.Instance.ChangeSceneTo(SManager.Instance.Scene, null));
    }
}
