﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;

public class SelectionManager : Singleton<SelectionManager>
{
    #region Properties

    #endregion

    #region Publics

    public MRobberPlayerEvent OnArmRobberPlayerJoined;
    public MRobberPlayerEvent OnArmRobberPlayerLeaved;

    public string JoinAction;
    public string LeaveAction;
    public int maxPlayers = 4;

    #endregion

    #region Privates

    private Dictionary<Guid, MRobberPlayer> playersJoined = new Dictionary<Guid, MRobberPlayer>();

    #endregion

    private void Start()
    {
        if (LobbyManager.Instance != null)
        {
            LobbyManager.Instance.OnConnectedToLobby.AddListener(HandlePlayerJoin);
            LobbyManager.Instance.OnDisconnectedFromLobby.AddListener(HandlePlayerLeave);
        }

        InputManager.Instance.OnControllerConnected += ControllerConnected;
        InputManager.Instance.OnControllerDisconnected += ControllerDisconnected;

        CheckFirstPlayer();

        //Controlla se ci sono già giocatori 
        if (ModelContainer.Players.Count == 0) return;

        foreach (MRobberPlayer p in ModelContainer.Players)
        {
            if (p == null) continue;

            HandlePlayerJoin(p);
        }
    }

    private void Update()
    {
        // Watch for JoinGame action in each Player
        for (int i = 0; i < InputManager.Instance.MaxPlayersCapacity; i++)
        {
            if (i == 0) continue;

            var player = InputManager.Instance.GetPlayerFromID(i);

            if (player == null) continue;

            if (player.GetButtonDown(JoinAction))
                AssignNextPlayer(player);
            else if (player.GetButtonDown(LeaveAction))
                RemovePlayer(player);
        }
    }

    private void OnDestroy()
    {
        if (!InputManager.IsInstanced()) return;

        InputManager.Instance.OnControllerConnected -= ControllerConnected;
        InputManager.Instance.OnControllerDisconnected -= ControllerDisconnected;

        if (!LobbyManager.IsInstanced()) return;

        LobbyManager.Instance.OnConnectedToLobby.RemoveListener(HandlePlayerJoin);
        LobbyManager.Instance.OnDisconnectedFromLobby.RemoveListener(HandlePlayerLeave);

    }

    public void CheckFirstPlayer()
    {
        StartCoroutine(CheckingRoutine());
    }

    private void ControllerConnected(ControllerStatusChangedEventArgs obj)
    {
        CheckFirstPlayer();
    }

    private void ControllerDisconnected(ControllerStatusChangedEventArgs obj)
    {
        RemovePlayer(InputManager.Instance.GetPlayerFromID(obj.controllerId));
    }

    private void AssignNextPlayer(Player rewiredPlayer)
    {
        // Se si è raggiunto il numero massimo di giocatori non è possibile aggiungerne un'altro
        if (ModelContainer.Players.Count >= maxPlayers) return;

        //prende il primo controller 
        var controller = rewiredPlayer.controllers.Controllers.ToArray()[0];

        // TODO: controllare se anche il controller 0 non è già assegnato ad un'altro player
        if (ModelContainer.Players.Exists(p => p.RewiredPlayer.id == rewiredPlayer.id)) return;

        // Tutto è ok, per cui crea un nuovo armrobberplayer e joinalo
        // per ora crea un nome random, crea l'id e assegna il rewired player
        var armPlayer = new MRobberPlayer($"Player_{UnityEngine.Random.Range(1,9999)}",
                                            Guid.NewGuid(),
                                            rewiredPlayer);
        HandlePlayerJoin(armPlayer);

#if UNITY_EDITOR
        Debug.Log("Added Rewired Player id " + rewiredPlayer.id);
#endif
    }

    private void HandlePlayerJoin(MRobberPlayer player)
    {
        if (playersJoined.ContainsKey(player.Id))
        {
#if UNITY_EDITOR
            Debug.Log("Giocatore già aggiunto");
#endif
            return;
        }

        // Aggiungi il player al model container
        if(!ModelContainer.AlreadyExist(player.Id))
            ModelContainer.Players.Add(player);
        
        playersJoined.Add(player.Id,player);
        // invoca l'evento
        OnArmRobberPlayerJoined?.Invoke(player);
    }

    private void HandlePlayerLeave(MRobberPlayer player)
    {
        if (!playersJoined.ContainsKey(player.Id))
        {
#if UNITY_EDITOR
            Debug.Log("Giocatore inesistente");
#endif
            return;
        }

        // Rimuovi il player al model container
        ModelContainer.Players.Remove(player);
        playersJoined.Remove(player.Id);
        // invoca l'evento
        OnArmRobberPlayerLeaved?.Invoke(player);
    }

    private void RemovePlayer(Player rewiredPlayer)
    {
        if(rewiredPlayer == null)
        {
            Debug.LogWarning("Rewired player is null");
            return;
        }

        // cannot remove the first player
        if (rewiredPlayer.id == 0) return;

        var armPlayer = ModelContainer.Players.Find(p => p.RewiredPlayer.id == rewiredPlayer.id);

        if (armPlayer == null)
        {
            Debug.LogWarning($"Hmm qualcosa non va! || Player {rewiredPlayer.id} ");
            return;
        }

        HandlePlayerLeave(armPlayer);

        Debug.Log("Armrobber player leaved"); 
    }

    private IEnumerator CheckingRoutine()
    {
        yield return new WaitForEndOfFrame();

        AssignNextPlayer(InputManager.Instance.GetPlayerFromID(0));
        
        StartCoroutine(InputManager.Instance.CheckingRoutine("UI", new string[] 
        {
            "Assignament",
            "Default"
        }));

        yield return null;

    }
}