﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

// TODO : Sync all playersmanager
public class PlayerManager : PunCallbackSingleton<PlayerManager>, IObservable, IOnEventCallback
{
    #region Properties

    public int NumPlayer => ModelContainer.Players.Count;

    #endregion

    #region Inspector_Fields

    public List<PlayerComponent> PlayersAlive => PlayersInGame.FindAll(p => !p.IsPlayerDead);

    [SerializeField][Sirenix.OdinInspector.ReadOnly]
    public List<PlayerComponent> PlayersInGame = new List<PlayerComponent>();
    #endregion

    #region Publics

    public UnityEvent OnAllPlayersCreated = new UnityEvent();

    public GameObjectEvent OnGameObjectCreated = new GameObjectEvent();

    public Action<MRobberPlayer> OnArmRobberPlayerAssociated;

    public Action OnAllPlayersLoaded;

    #endregion

    #region Privates

    [SerializeField][Sirenix.OdinInspector.ShowInInspector][Sirenix.OdinInspector.ReadOnly]
    private Dictionary<string, List<BehaviourBase>> _playersBehaviours = 
                      new Dictionary<string, List<BehaviourBase>>();

    [Sirenix.OdinInspector.ReadOnly][SerializeField]
    private GameObject _playerWinner;
    [Sirenix.OdinInspector.ReadOnly][SerializeField]
    private bool _alreadyInitialized = false;
    #endregion

    private void Awake()
    {
        //DontDestroyOnLoad(this);

        PhotonNetwork.AddCallbackTarget(this);
        GameManager.Instance.OnGameReady.AddListener(GameReady);
        SManager.Instance.OnSceneLoaded += SceneChanged;
        ArmNetworkManager.Instance.OnDisconnectedFromLobby.AddListener(PlayerLeft);

        StartListening();
    }

    protected override void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);

        PlayersInGame.Clear();
        
        if (GameManager.IsInstanced())
            GameManager.Instance?.OnGameReady?.RemoveListener(GameReady);

        OnArmRobberPlayerAssociated = null;

        OnAllPlayersCreated.RemoveAllListeners();
        OnGameObjectCreated.RemoveAllListeners();

        if (SManager.IsInstanced())
            SManager.Instance.OnSceneLoaded -= SceneChanged;
        
        StopListening();

        base.OnDestroy();
    }

    public T GetComponentFrom<T>(string playerName) where T : BehaviourBase
    {
        var collection = _playersBehaviours[playerName];

        var component = collection.Find(c => c?.GetObject.GetType() == typeof(T));

        return (T)component;
    }

    private void PlayerLeft(MRobberPlayer player)
    {
        ModelContainer.Players.RemoveAll(p => p.Id == player.Id);

        if (!PhotonNetwork.IsMasterClient) return;

        List<int> indices = new List<int>();

        for (int i = 0; i < PlayersInGame.Count; i++)
        {
            PlayerComponent playerComponent = PlayersInGame[i];

            if (playerComponent.PlayerModel.Id != player.Id) continue;

            PhotonNetwork.DestroyPlayerObjects(playerComponent.photonView.Controller);

            indices.Add(i);
        }

        foreach (var index in indices)
            PlayersInGame.RemoveAt(index);
    }

    private void GameReady()
    {
        Debug.Log("Game Ready Player manager");

        if (_alreadyInitialized) return;

        if (ModelContainer.Players.Count > 0)
        {
#if UNITY_EDITOR
            if (GameManager.Instance.Debug) return;
#endif

            if(PhotonNetwork.IsMasterClient)
                InstantiatePlayers();
            else
            {
                foreach (var playerModel in ModelContainer.GetAllLocalPlayers())
                {
                    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                    PhotonNetwork.RaiseEvent(PUNEventCodes.REQUEST_SPAWN_PLAYER, playerModel.Name, raiseEventOptions, SendOptions.SendReliable);
                }
            }
        }

#if UNITY_EDITOR
        CreatePlayersInDebug();
#endif

        OnAllPlayersCreated?.Invoke();

        _alreadyInitialized = true;
    }

    public void StartListening()
    {
        ObserverEventManager.AddObserver(ObserverEventNames.DEATH_EVENT,this);
    }

    public void StopListening()
    {
        ObserverEventManager.RemoveObserver(ObserverEventNames.DEATH_EVENT, this);
    }

    public void OnEvent(string eventName, object objectPassed)
    {
        PlayerComponent player = (PlayerComponent)objectPassed;

        if (player == null) return;

        CheckVictory();
    }

    private void CheckVictory()
    {
        // se un solo giocatore è rimasto, allora quello rimasto ha vinto, cerca il giocatore rimasto vivo
        if(PlayersInGame.FindAll(p => !p.IsPlayerDead).Count == 1)
        {
            _playerWinner = PlayersInGame.Find(p => !p.IsPlayerDead)?.gameObject;
        }

        if (_playerWinner == null) return;

        // se l'ha trovato lancia l'evento e setta lo stato di vittoria
        ObserverEventManager.Trigger<GameObject>(ObserverEventNames.WIN_EVENT, _playerWinner);

        GameManager.Instance.State = GameState.VictoryPause;
    }

    private void InstantiatePlayers()
    {
        foreach (var player in ModelContainer.GetAllLocalPlayers())
        {
            if (player == null) continue;

            var model = player.RobotModel;

            if (model == null) continue;

            GameObject go = null;

            go = PhotonNetwork.Instantiate(model.RobotPath, EnvironmentManager.Instance.GetPlayerSpawn().position, Quaternion.identity);
           
            FinalizePlayerCreation(go, player);
        }
    }

    public void FinalizePlayerCreation(GameObject go,MRobberPlayer player)
    {
        Debug.Log("Finalize player creation " + player.Name);

        go.name = player.Name;

        var behaviours = go.GetComponents<BehaviourBase>().ToList();

        if(!_playersBehaviours.ContainsKey(player.Name))
            _playersBehaviours.Add(player.Name, behaviours);

        PlayerComponent pComponent = GetComponentFrom<PlayerComponent>(player.Name);

        if (!PlayersInGame.Exists(p => p.PlayerModel.Id == player.Id))
        {
            Debug.Log($"(Player Manager) Adding player to player list {player.Name}");

            PlayersInGame.Add(pComponent);
        }

        foreach (var behaviour in behaviours)
        {
            if (behaviour == null) continue;

            behaviour.Initialize(player.RobotModel);
        }

#if UNITY_EDITOR
        if (!GameManager.Instance.Debug)
        {
#endif
            if (player.IsLocal)
            {

                pComponent.PhotonView.RPC("RPC_SendModel", RpcTarget.Others, MRobberPlayer.Serialize(player));
            }
            else
                Debug.Log("Is not local so i don't send the model");
#if UNITY_EDITOR
        }
#endif

        Debug.Log("Created GO and sent event for " + player.Name);

        var sComponent = GetComponentFrom<ShootingComponent>(player.Name);

        sComponent?.PrepareForBattle();

        OnArmRobberPlayerAssociated?.Invoke(player);
        OnGameObjectCreated?.Invoke(go);

        if (PlayersInGame.Count == ModelContainer.Players.Count)
            OnAllPlayersLoaded?.Invoke();
    }

    private void SceneChanged(SceneName scene)
    {
        if (scene == SceneName.Test_Environment) return;

        Debug.Log($" SCENE CHANGED {scene}");

        Clear();
    }

    public void Clear()
    {
        _alreadyInitialized = false;
        _playersBehaviours.Clear();
        _playerWinner = null;
        PlayersInGame.Clear();

        OnAllPlayersCreated.RemoveAllListeners();
        OnGameObjectCreated.RemoveAllListeners();
        OnArmRobberPlayerAssociated = null;
        OnAllPlayersLoaded = null;
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code != PUNEventCodes.SPAWN_PLAYER_EVENT) return;

        Debug.Log("Spawn player ");

        var hash = (ExitGames.Client.Photon.Hashtable)photonEvent.CustomData;

        List<Vector3> vector3s = new List<Vector3>();

        string playerName = string.Empty;

        foreach (var k in hash.Keys)
        {
            if (k is string == false) continue;

            if (string.IsNullOrEmpty(playerName))
            {
                string key = k.ToString();
                int index = key.IndexOf('*');
                playerName = key.Substring(0, index);
            }

            if (ModelContainer.GetAllLocalPlayers().Exists(p => p.Name == playerName))
            {
                vector3s.Add((Vector3)hash[k]);
            }
            else
                return;
        }

        foreach (var playerModel in ModelContainer.GetAllLocalPlayers())
        {
            if (playerModel == null) continue;
            if (playerModel.Name != playerName) continue;

            var go = PhotonNetwork.Instantiate(playerModel.RobotModel.RobotPath, vector3s[0], Quaternion.identity);

            FinalizePlayerCreation(go, playerModel);
        }
    }

#if UNITY_EDITOR
    private void CreatePlayersInDebug()
    {
        if (GameManager.Instance.Debug && ModelContainer.Players.Count <= 0)
        {
            int i = 0;
            foreach (var player in FindObjectsOfType<PlayerComponent>())
            {
                ModelContainer.Players.Add(new MRobberPlayer($"Player_{i}",
                                                               Guid.NewGuid(),
                                                               InputManager.Instance.GetPlayerFromID(i)));

                FinalizePlayerCreation(player.gameObject,ModelContainer.Players[i]);

                i++;
            }
        }
    }
#endif

}