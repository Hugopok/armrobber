﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;


// Questa classe sarà responabile degli eventi e degli oggetti che saranno in scena
public class EnvironmentManager : PunCallbackSingleton<EnvironmentManager>, IOnEventCallback, IObservable
{   
    [HideInWindow]
    public List<Transform> _playerSpawnPostions = new List<Transform>();
    [SerializeField][HideInWindow]
    private MGlobalGameplaySettings _settings;

    [Header("Spawn")]
    [SerializeField] [HideInWindow] protected int playerSpawned = 0;

    [Header("PowerUp And Weapons Settings")]
    [SerializeField] private float _spawnEverySecond = 10;
    [SerializeField] [HideInWindow] private TransformBoolDictionary _powerUpAndArmSpawner = new TransformBoolDictionary();
    [SerializeField] [HideInWindow] private List<SpawnableTypeClass> _powerUpsAndweapons = new List<SpawnableTypeClass>();

    private void Start()
    {
        _settings = SettingsManager.Instance.GameplaySettings;

#if UNITY_EDITOR
        if (!GameManager.Instance.Debug && !PhotonNetwork.IsMasterClient) return;
#else
        if (!PhotonNetwork.IsMasterClient) return;
#endif
        _instance = this;
        
        StartListening();
        ArmNetworkManager.Instance.OnCountDownFinished += SpawnWeapon;
        StartCoroutine(SpawnRoutine());
        StartCoroutine(ClearRoutine());
    }

    public Transform GetPlayerSpawn()
    {
        if (playerSpawned < 0 || playerSpawned > _playerSpawnPostions.Count - 1) return null;

        Transform pos = _playerSpawnPostions[playerSpawned];

        playerSpawned++;

        return pos;
    }
    
    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code != PUNEventCodes.REQUEST_SPAWN_PLAYER) return;

        Debug.Log("Spawn event recieved " + playerSpawned);

        PhotonNetwork.IsMessageQueueRunning = false;

        string playerName = (string)photonEvent.CustomData;

        var hash = new ExitGames.Client.Photon.Hashtable();

        Transform tr = GetPlayerSpawn();

        hash.Add($"{playerName}*P", tr.position);
        hash.Add($"{playerName}*R", tr.rotation.eulerAngles);

        var sendOpt = SendOptions.SendReliable;

        PhotonNetwork.RaiseEvent(PUNEventCodes.SPAWN_PLAYER_EVENT,
                                 hash,
                                 new RaiseEventOptions() { Receivers = ReceiverGroup.Others },
                                 sendOpt);

        PhotonNetwork.IsMessageQueueRunning = true;
    }
    
    public void BalanceWeaponWhenPlayerLeaveOrDie(MRobberPlayer player)
    {
        if (PlayerManager.Instance.PlayersAlive.Count == 0) return;

        var shootingComponent = PlayerManager.Instance.GetComponentFrom<ShootingComponent>(player.Name);

        if(shootingComponent.GetArmsCount >= 0)
        {
            foreach (var linker in shootingComponent.GetLinkers)
            {
                if (linker == null || linker.armWeapon == null) continue;

                PhotonNetwork.Destroy(linker.armWeapon.gameObject);
            }
        }

        int playersInGame = PlayerManager.Instance.PlayersAlive.Count;

        var wPIngame = _settings.MaxWeaponsInGame.Find(p => p.CurrentPlayers == playersInGame);

        var weaponsInGame = FindObjectsOfType<ArmWeapon>();

        if (weaponsInGame == null || wPIngame == null) return;

        if (wPIngame.MaxWeapons == weaponsInGame.Length) return;

        int difference = wPIngame.MaxWeapons - weaponsInGame.Length;

        if(difference > 0)
        {
            for (int i = 0; i < difference; i++)
            {
                SpawnWeapon();
            }
            
        }
        else if(difference < 0)
        {
            foreach (var p in PlayerManager.Instance.PlayersAlive)
            {
                var armsLinked = p.ShootingComponent.GetLinkers.FindAll(l => l.armWeapon != null);

                if (p == null || p.ShootingComponent == null) continue;
                if (armsLinked.Count <= 2) continue;

                // Toglie solo l'ultimo braccio
                PhotonNetwork.Destroy(armsLinked[armsLinked.Count - 1].armWeapon.gameObject);
            }

            weaponsInGame = FindObjectsOfType<ArmWeapon>();

            difference = wPIngame.MaxWeapons - weaponsInGame.Length;

            if (difference >= 0) return;

            weaponsInGame = weaponsInGame.ToList().FindAll(w => !w.IsMounted).ToArray();

            for (int i = 0; i < Mathf.Abs(difference); i++)
            {
                var weapon = weaponsInGame[i];

                if (weapon.IsMounted) continue;

                PhotonNetwork.Destroy(weapon.gameObject);
            }
        }
    }

    private IEnumerator SpawnRoutine()
    {
        yield return new WaitForSeconds(_spawnEverySecond);

        while (_instance != null)
        {
            WeaponsPerPlayer wPerPlayers = null;

            while (wPerPlayers == null)
            {
                wPerPlayers = _settings.MaxWeaponsInGame.Find(wp => wp.CurrentPlayers == PlayerManager.Instance.PlayersAlive.Count);

                yield return null;
            } 

            if (FindObjectsOfType<ArmWeapon>().Length < wPerPlayers.MaxWeapons)
            {
                wPerPlayers = _settings.MaxWeaponsInGame.Find(wp => wp.CurrentPlayers == PlayerManager.Instance.PlayersAlive.Count);

                yield return new WaitForSeconds(_spawnEverySecond);

                SpawnWeapon();
            }

            yield return null;
        }
    }

    private void SpawnWeapon()
    {
        var list = _powerUpsAndweapons.FindAll(p => p.spawnableType == SpawnableType.Weapon);

        var spawnable = list[Random.Range(0, list.Count)];

        var traPos = GetRandomTransform();

        var weapon = PhotonNetwork.InstantiateRoomObject(spawnable.path, traPos.position, Quaternion.identity);

        weapon.GetComponent<Smooth.SmoothSyncPUN2>().teleportAnyObject(traPos.position,Quaternion.identity,weapon.transform.localScale);
    }

    private IEnumerator ClearRoutine()
    {
        while (_instance != null)
        {
            yield return new WaitForSeconds(_spawnEverySecond * _powerUpAndArmSpawner.Count - 1);

            List<Transform> keys = new List<Transform>();

            foreach (var k in _powerUpAndArmSpawner.Keys)
            {
                keys.Add(k);
            }

            foreach (var k in keys)
            {
                _powerUpAndArmSpawner[k] = true;
            }

            yield return null;
        }
    }
    
    private IEnumerator Wait(Transform key)
    {
        yield return new WaitForSeconds(_spawnEverySecond);

        if (_powerUpAndArmSpawner.ContainsKey(key))
            _powerUpAndArmSpawner[key] = false;

    }
    
    private List<string> GetWeapons()
    {
        List<string> paths = new List<string>();


        var list = _powerUpsAndweapons.FindAll(objectT => objectT.spawnableType == SpawnableType.Weapon);

        if (list.Count == 0) return paths;

        foreach (var obj in list)
        {
            paths.Add(obj.path);
        }
        return paths;
    }

    private Transform GetRandomTransform()
    {
        var possiblePositions = new List<Transform>();

        foreach (var kv in _powerUpAndArmSpawner)
        {
            if (kv.Value == true) continue;

            possiblePositions.Add(kv.Key);
        }

        if(possiblePositions.Count == 0)
        {
            return _playerSpawnPostions[0];
        }

        var pos = possiblePositions[Random.Range(0, possiblePositions.Count)];

        _powerUpAndArmSpawner[pos] = true;

        return pos;
    }

    public void StartListening()
    {
        ObserverEventManager.AddObserver(ObserverEventNames.DEATH_EVENT, this);
        ObserverEventManager.AddObserver(ObserverEventNames.WIN_EVENT,this);
    }

    public void StopListening()
    {
        ObserverEventManager.RemoveObserver(ObserverEventNames.DEATH_EVENT, this);
        ObserverEventManager.RemoveObserver(ObserverEventNames.WIN_EVENT,this);
    }

    public void OnEvent(string eventName, object objectPassed)
    {
        if (eventName == ObserverEventNames.DEATH_EVENT)
        {
            PlayerComponent component = objectPassed as PlayerComponent;

            BalanceWeaponWhenPlayerLeaveOrDie(component.PlayerModel);
        }
        else if(eventName == ObserverEventNames.WIN_EVENT)
        {
            StopAllCoroutines();
        }
    }

    protected override void OnDestroy()
    {
        StopAllCoroutines();
        base.OnDestroy();
        StopListening();
        if(ArmNetworkManager.IsInstanced())
            ArmNetworkManager.Instance.OnCountDownFinished -= SpawnWeapon;
        
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        foreach (var item in _playerSpawnPostions)
        {
            Gizmos.DrawSphere(item.position,0.35f);
        }

        Gizmos.color = Color.yellow;

        foreach (var item in _powerUpAndArmSpawner)
        {
            Gizmos.DrawSphere(item.Key.position, 0.35f);
        }
    }
#endif
}

[System.Serializable]
public class SpawnableTypeClass
{
    public string path;
    public SpawnableType spawnableType;
}

public enum SpawnableType
{
    Weapon,
    PowerUp
}