﻿using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    public BasePanel CurrentPanelOpened => _currentPanelOpened;

    [SerializeField]
    private List<BasePanel> _allPanels = new List<BasePanel>();

    [Sirenix.OdinInspector.ReadOnly][SerializeField]
    private BasePanel _currentPanelOpened;

    private void Start()
    {
        GameManager.Instance.OnPauseUpdate += PauseUpdateLoop;

        if(_allPanels.Count > 0)
            _currentPanelOpened = _allPanels[0];

        for (int i = 1; i < _allPanels.Count; i++)
        {
            _allPanels[i].OpenClose(false);
            _allPanels[i].OnClosed += RefreshWhenPanelClose;
        }

        if (_currentPanelOpened != null && _currentPanelOpened.OpenAtStart)
            _currentPanelOpened?.OpenClose(true);

        GameManager.Instance.OnStateChanged += StateChanged;

        GameObject.Find("IngameDebugConsole")?.SetActive(true);
    }

    public void AddPanel(BasePanel basePanel)
    {
        if (_allPanels.Exists(p => p.GetInstanceID() == basePanel.GetInstanceID())) 
            return;

        _allPanels.Add(basePanel);
    }

    public void RemovePanel(BasePanel basePanel)
    {
        if (!_allPanels.Exists(p => p.GetInstanceID() == basePanel.GetInstanceID())) return;

        _allPanels.Remove(basePanel);
    }

    public void OpenNextSubPanel(string subPanelName)
    {
        var t = _currentPanelOpened;

        _currentPanelOpened.OpenClose(false);

        _currentPanelOpened = _currentPanelOpened.GetSubPanel(subPanelName);

        if (_currentPanelOpened == null)
        {
#if UNITY_EDITOR
            Debug.LogWarning($"No sub panel found with name {subPanelName}");
#endif
            t.OpenClose(true);
            return;
        }

        _currentPanelOpened.OpenClose(true);
    }

    public void OpenNextSubPanel<T>() where T : SubPanel
    {
        var t = _currentPanelOpened;

        _currentPanelOpened.OpenClose(false);

        _currentPanelOpened = (T)_currentPanelOpened.GetSubPanel<T>();

        if (_currentPanelOpened == null)
        {
#if UNITY_EDITOR
            Debug.LogWarning($"No sub panel found typeof {typeof(T).Name}");
#endif
            t.OpenClose(true);
            return;
        }

        _currentPanelOpened.OpenClose(true);
    }

    public void OpenPanel<T>() where T : BasePanel
    {
        var t = _currentPanelOpened;

        if (_currentPanelOpened != null)
            _currentPanelOpened.OpenClose(false);

        _currentPanelOpened = (T)_allPanels.Find(p => p.GetType() == typeof(T));

        if (_currentPanelOpened == null)
        {
#if UNITY_EDITOR
            Debug.LogWarning($"No sub panel found typeof {typeof(T).Name}");
#endif
            if (t != null)
                t.OpenClose(true);
            
            return;
        }

        if (_currentPanelOpened != null)
            _currentPanelOpened.OpenClose(true);
    }

    public void OpenPreviousPanel()
    {
        var p = _currentPanelOpened as SubPanel;

        if (p == null) return;

        _currentPanelOpened.OpenClose(false);

        _currentPanelOpened = p.PreviousPanel;

        _currentPanelOpened.OpenClose(true);
    }

    private void PauseUpdateLoop()
    {
        if (InputManager.Instance.IsUICancelPressed())
        {
            OpenPreviousPanel();
        }
    }

    private void RefreshWhenPanelClose(BasePanel panel)
    {
        if (panel == _currentPanelOpened)
            _currentPanelOpened = null;
    }

    private void StateChanged(GameState state)
    {
        if (state == GameState.Pause)
            OpenPanel<PauseMenu>();
    }
}
