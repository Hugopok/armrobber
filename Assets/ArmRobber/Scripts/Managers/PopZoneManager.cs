﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopZoneManager : PunCallbackSingleton<PopZoneManager>
{
    [SerializeField]
    private List<BoxCollider> _zones = new List<BoxCollider>();
    [SerializeField]
    private float _maxDistance;
    [SerializeField]
    private float _minDistance = 10;

#if UNITY_EDITOR
    private Collider _colSelected;
    private Vector3 _point;
#endif
    private void Start()
    {
        // per sicurezza
        foreach (var col in _zones)
        {
            col.isTrigger = true;
        }
    }

    public Vector3 GetRandomPointFrom(Vector3 source)
    {
       return GetRandomPointInCollider(GetZoneWithMaxDistance(source));
    }

    Vector3 GetRandomPointInCollider(Collider collider)
    {
        var bounds = collider.bounds;

        float offsetX = Random.Range(-bounds.extents.x, bounds.extents.x);
        float offsetY = Random.Range(-bounds.extents.y, bounds.extents.y);
        float offsetZ = Random.Range(-bounds.extents.z, bounds.extents.z);

        Vector3 point = bounds.center + new Vector3(offsetX, offsetY, offsetZ);
#if UNITY_EDITOR
        var debugPoint = new GameObject($"Point_{collider.gameObject.name}");
        debugPoint.transform.position = point;
        Debug.Log(debugPoint.name,debugPoint);
        _colSelected = collider;
        _point = point;
#endif
        return point;
    }

    private BoxCollider GetZoneWithMaxDistance(Vector3 source)
    {
        float distance = -Mathf.Infinity;
        BoxCollider zone = null;
        BoxCollider playerZone = null;

        Ray ray = new Ray(new Vector3(source.x,CameraManager.Instance.PopTransformPoint.position.y,source.z),Vector3.down);

        foreach (var curZone in _zones)
        {
            // se è nella stessa zona non lo calcola
            if (curZone.bounds.IntersectRay(ray))
            {
                playerZone = curZone;
                continue;
            }

            float zoneDistance = Vector3.Distance(curZone.bounds.center,source);

            if (zoneDistance <= _minDistance) continue;
            if (zoneDistance > _maxDistance) continue;

            if (zoneDistance > distance)
            {
                distance = zoneDistance;
                zone = curZone;
            }
        }

        if(zone == null)
        {
            Debug.LogWarning("Prendo un punto casuale del popping");

            List<BoxCollider> newZones = new List<BoxCollider>(_zones);

            if (playerZone != null)
                newZones = _zones.FindAll(c => c != playerZone);
            
            zone = newZones[Random.Range(0, newZones.Count)];
        }

        return zone;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (_colSelected == null) return;

        Gizmos.color = Color.yellow;

        Gizmos.DrawCube(_colSelected.bounds.center,_colSelected.bounds.size);

        Gizmos.color = Color.red;

        Gizmos.DrawSphere(_point,0.14f);
    }
#endif
}
