﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System;

public class ArmNetworkManager : ANetworkBase<ArmNetworkManager>
{
    public int PlayersConnected => PlayerManager.Instance.PlayersInGame.Count;
    public int SendRate => _sendRate;
    public int SerializationRate => _serializationRate;

    public Action<string> OnCountDown;
    public Action OnCountDownFinished;

    [Header("PhotonSettings")]

    [SerializeField]
    private int _sendRate;
    [SerializeField]
    private int _serializationRate;

    [Header("Miscellaneous")]

    [SerializeField]
    private bool _dontDestroyOnLoad = true;

    private void Awake()
    {
        if(IsInstanced() && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;

        if (_dontDestroyOnLoad)
            DontDestroyOnLoad(gameObject);

        SManager.Instance.OnSceneLoaded += SceneLoaded;

        StartCoroutine(WaitPhotonNetwork());

    }

    private void Update()
    {
        if (!PhotonNetwork.OfflineMode)
        {
            if (_sendRate != PhotonNetwork.SendRate || _serializationRate != PhotonNetwork.SerializationRate)
            {
                PhotonNetwork.SerializationRate = _serializationRate;
                PhotonNetwork.SendRate = _sendRate;

                foreach (var smooth in FindObjectsOfType<Smooth.SmoothSyncPUN2>())
                {
                    if (smooth == null) continue;

                    smooth.interpolationBackTime = (float)(1.0f / (float)_sendRate);
                }
            }
        }
    }

    private IEnumerator WaitPhotonNetwork()
    {
        yield return PhotonNetwork.IsConnectedAndReady;

        Debug.Log($"Original Send rate {PhotonNetwork.SendRate} || Original serialization rate {PhotonNetwork.SerializationRate}");

        PhotonNetwork.SerializationRate = _serializationRate;
        PhotonNetwork.SendRate = _sendRate;
    }

    #region Photon

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (otherPlayer.IsInactive) return;

        PerformLeft(otherPlayer);
    }

    protected override void PlayerJoin(MRobberPlayer model)
    {
    }

    protected override void PlayerLeft(MRobberPlayer model)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        if (!EnvironmentManager.IsInstanced()) return;

        EnvironmentManager.Instance.BalanceWeaponWhenPlayerLeaveOrDie(model);
    }

    #endregion

    private void SceneLoaded(SceneName scene)
    {
        if (scene == SceneName.MainMenu) return;

        if(!PlayerManager.IsInstanced())
        {
            Debug.LogError("Playermanager is not instanced ");
            return;
        }

        GameManager.Instance.State = GameState.LoadingMap;

        PlayerManager.Instance.OnAllPlayersLoaded += AllPlayersLoaded;
    }

    private void AllPlayersLoaded()
    {
        StartCoroutine(CounterRoutine());
    }

    IEnumerator CounterRoutine()
    {
        yield return new WaitForSecondsRealtime(0.5f);

        for (int i = 3; i >= 0; i--)
        {
            Debug.Log($"Robber in {i}");

            string text = string.Empty;

            if (i == 0)
                text = "Robber";
            else
                text = i.ToString();

            OnCountDown?.Invoke(text);

            yield return new WaitForSecondsRealtime(1);
        }

        OnCountDownFinished?.Invoke();

        GameManager.Instance.State = GameState.InGame;

        PlayerManager.Instance.OnAllPlayersLoaded -= AllPlayersLoaded;
    }
}
