﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : Singleton<SettingsManager>
{
    public MGlobalGameplaySettings GameplaySettings => _gameplaySettings;

    [SerializeField]
    private MGlobalGameplaySettings _gameplaySettings;

}
