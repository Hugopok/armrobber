﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ANetworkBase<T> : PunCallbackSingleton<T> where T : MonoBehaviour
{
    [Sirenix.OdinInspector.ShowInInspector]
    [Sirenix.OdinInspector.ReadOnly]
    public bool IsConnected { get; protected set; }

    public MRobberPlayerEvent OnConnectedToLobby = new MRobberPlayerEvent();
    public MRobberPlayerEvent OnDisconnectedFromLobby = new MRobberPlayerEvent();

    public override void OnConnectedToMaster()
    {
        IsConnected = true;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PerformLeft(otherPlayer);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PerformJoin(newPlayer);
    }

    protected virtual void PlayerLeft(MRobberPlayer model) { }
    protected virtual void PlayerJoin(MRobberPlayer model) { }

    protected virtual void PerformLeft(Player player)
    {
        string[] names = (string[])player.CustomProperties["names"];
        string[] ids = (string[])player.CustomProperties["ids"];
        string[] robots = (string[])player.CustomProperties["robots"];

        for (int i = 0; i < names.Length; i++)
        {
            var model = new MRobberPlayer(names[i], Guid.Parse(ids[i]), rewiredPlayer: null);
            PlayerLeft(model);
            OnDisconnectedFromLobby?.Invoke(model);
        }
    }

    protected virtual void PerformJoin(Player player) 
    {
        string[] names = (string[])player.CustomProperties["names"];
        string[] ids = (string[])player.CustomProperties["ids"];
        string[] robots = (string[])player.CustomProperties["robots"];

        for (int i = 0; i < names.Length; i++)
        {
            var model = new MRobberPlayer(names[i], Guid.Parse(ids[i]), rewiredPlayer: null);
            PlayerJoin(model);
            OnConnectedToLobby?.Invoke(model);
        }

        if (!PhotonNetwork.IsMasterClient) return;
    }
}
