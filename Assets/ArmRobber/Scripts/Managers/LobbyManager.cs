﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class LobbyManager : ANetworkBase<LobbyManager>
{   
    public List<RoomInfo> Rooms => _rooms;

    public Action OnConnectedToMasterClient;
    public Action OnOfflineModeActivated;
    public Action<List<RoomInfo>> OnRoomsUpdated;
    
    private List<RoomInfo> _rooms = new List<RoomInfo>();
    private bool _isOfflineModeActivated = false;

    [Header("Settings")]
    [SerializeField]
    private int _maxPlayers = 4;

    protected void Start()
    {
        OnOfflineModeActivated += CreateDefaultRoom;

        PhotonNetwork.ConnectUsingSettings();

        PhotonNetwork.AutomaticallySyncScene = true;
    }

    protected void OnApplicationQuit()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
    }

    public void OfflineMode(bool active)
    {
        if (active)
            PhotonNetwork.Disconnect();
    }

    #region Photon Methods

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        if(!_isOfflineModeActivated)
            PhotonNetwork.JoinLobby();

        OnConnectedToMasterClient?.Invoke();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        switch (cause)
        {
            case DisconnectCause.DisconnectByServerLogic:
                PhotonNetwork.OfflineMode = true;
                _isOfflineModeActivated = true;
                OnOfflineModeActivated?.Invoke();
                break;
            case DisconnectCause.DisconnectByClientLogic:
                PhotonNetwork.OfflineMode = true;
                _isOfflineModeActivated = true;
                OnOfflineModeActivated?.Invoke();
                break;
            default:
                break;
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("Player enter : " + newPlayer.ActorNumber);

        if (ModelContainer.Players.Count >= _maxPlayers)
        {
            PhotonNetwork.CurrentRoom.Players.Remove(newPlayer.ActorNumber);
           
            PhotonNetwork.CurrentRoom.IsOpen = false;
           
            return;
        }

        PerformJoin(newPlayer);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PerformLeft(otherPlayer);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Room joined");

        foreach (var player in PhotonNetwork.PlayerListOthers)
        {
            PerformJoin(player);
        }

        base.OnJoinedRoom();
    }

    public override void OnLeftRoom()
    {
        foreach (var player in ModelContainer.GetAllRemotePlayers())
        {
            OnDisconnectedFromLobby?.Invoke(player);
        }

        ModelContainer.Players.RemoveAll(p => !p.IsLocal);

        base.OnLeftRoom();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.LogError($"Join failed {returnCode} || {message}");

        base.OnJoinRoomFailed(returnCode, message);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Room created");

        base.OnCreatedRoom();
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log("Room updated");

        foreach (var room in roomList)
        {
            int index =_rooms.FindIndex(r => r.Name == room.Name);

            if (index == -1)
            {
                _rooms.Add(room);
            }
            else
                _rooms[index] = room;
        }
        
        OnRoomsUpdated?.Invoke(_rooms);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogError($"Code : {returnCode} || {message}");

        base.OnCreateRoomFailed(returnCode, message);
    }

    #endregion

    #region Custom Methods

    public void CreateDefaultRoom()
    {
        CreateRoom(Guid.NewGuid().ToString(), new RoomOptions()
        {
            IsVisible = true,
            IsOpen = true,
            MaxPlayers = 4,
            CustomRoomProperties = new Hashtable() { { "Map", "Gameplay" } }
        });
    }

    public void CreateRoom(string lobbyName, RoomOptions roomOptions)
    {
        SetLocalPlayer();
        PhotonNetwork.CreateRoom(lobbyName,roomOptions);
    }

    public void JoinRandomRoom()
    {
        SetLocalPlayer();
        PhotonNetwork.JoinRandomRoom();
    }

    public void JoinRoomByName(string name)
    {
        SetLocalPlayer();
        PhotonNetwork.JoinRoom(name);
    }

    private void SetLocalPlayer()
    {
        var models = ModelsToSend();
        var props = new Hashtable();

        props.Add("names", new string[models.Count]);
        props.Add("ids", new string[models.Count]);
        props.Add("robots", new string[models.Count]);

        int index = 0;

        foreach (var model in models)
        {
            ((string[])props["names"])[index] = model.Name;
            ((string[])props["ids"])[index] = model.Id.ToString();
            ((string[])props["robots"])[index] = model.RobotModel.RobotName;

            index++;
        }

        PhotonNetwork.LocalPlayer.SetCustomProperties(props);

    }

    private List<MRobberPlayer> ModelsToSend()
    {
        var models = new List<MRobberPlayer>();

        foreach (var model in ModelContainer.GetAllLocalPlayers())
        {
            models.Add(new MRobberPlayer(model.Name, model.Id, rewiredPlayer:null));
        }

        return models;
    }

    #endregion
}