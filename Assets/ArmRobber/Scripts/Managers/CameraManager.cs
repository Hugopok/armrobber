﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : Singleton<CameraManager> ,IObservable
{
    public Transform PopTransformPoint => _popTransformPoint;
    public ProCamera2D ProCamera => _pCamera;

    [Header("Settings")]
    [SerializeField]
    private ShakePreset _shakeHitPreset;
    [SerializeField]
    private ShakePreset _shakeShootingPreset;
    [SerializeField]
    private Transform _popTransformPoint;

    [Header("DEBUG")]
    [Sirenix.OdinInspector.ReadOnly][SerializeField]
    private ProCamera2D _pCamera;

    private void Awake()
    {
        if (_pCamera == null)
            _pCamera = FindObjectOfType<ProCamera2D>();

        PlayerManager.Instance.OnGameObjectCreated.AddListener(AddTarget);
    }

    private void AddTarget(GameObject go)
    {
        if (PlayerManager.Instance.PlayersInGame.Count == 0) return;

        _pCamera.enabled = true;

        _pCamera.AddCameraTarget(go.transform);

        if (_pCamera.CameraTargets.Count >= PlayerManager.Instance.PlayersInGame.Count)
            StartListening();
    }

    public void StartListening()
    {
        ObserverEventManager.AddObserver(ObserverEventNames.DAMAGABLE_EVENT,this);
        ObserverEventManager.AddObserver(ObserverEventNames.SHOOTING_EVENT, this);
    }

    public void StopListening()
    {
        ObserverEventManager.RemoveObserver(ObserverEventNames.DAMAGABLE_EVENT, this);
        ObserverEventManager.RemoveObserver(ObserverEventNames.SHOOTING_EVENT, this);
    }

    private void OnDestroy()
    {
        StopListening();
        
        if (PlayerManager.IsInstanced())
            PlayerManager.Instance.OnGameObjectCreated.RemoveListener(AddTarget);
    }

    public void OnEvent(string eventName, object objectPassed)
    {
        switch (eventName)
        {
            case ObserverEventNames.SHOOTING_EVENT:
                ShakeCamera(_shakeShootingPreset);
                break;
            case ObserverEventNames.DAMAGABLE_EVENT:
                ShakeCamera(_shakeHitPreset);
                break;
            default:
                break;
        }   
    }

    private void ShakeCamera(ShakePreset preset)
    {
        if (preset!= null)
            ProCamera2DShake.Instance.Shake(preset);
    }
}
