﻿public enum GameState
{
    Intro,
    MainMenu,
    InGame,
    Matchmaking,
    LoadingMap,
    ClosingGame,
    Retry,
    Pause,
    VictoryPause
}