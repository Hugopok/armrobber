﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class DeathEffect : MonoBehaviour
{
    [SerializeField] private GameObject _objToAnimate;
    [SerializeField] private float _explosionRadius;
    [SerializeField] private float _explosionForce;
    [SerializeField] private DamagableComponent _damagableComponent;

    private bool _isEffectAlreadyDone;

    private void Awake()
    {
        _damagableComponent.OnDeath.AddListener(PopHead);
    }

    private void PopHead(DamagableComponent dmgComponent)
    {
        if (_isEffectAlreadyDone) return;
        if (_objToAnimate == null) return;

        _isEffectAlreadyDone = true;

        var collider = _objToAnimate.GetComponent<Collider>();

        if (collider == null)
            collider = _objToAnimate.AddComponent<BoxCollider>();

        _objToAnimate.transform.SetParent(null);

        StartCoroutine(WaitSetParent());
    }

    private IEnumerator WaitSetParent()
    {
        yield return new WaitForEndOfFrame();

        //forse aggiungere un timer dell'esplosione e un particle

        Rigidbody rb = _objToAnimate.AddComponent<Rigidbody>();

        rb.AddForce(new Vector3(2,2,1) * _explosionForce / 2,ForceMode.Impulse);

        rb.AddExplosionForce(_explosionForce, _objToAnimate.transform.position, _explosionRadius);

    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (_objToAnimate == null) return;

        Gizmos.color = Color.red;

        Gizmos.DrawSphere(_objToAnimate.transform.position,_explosionRadius);
    }
#endif
}
