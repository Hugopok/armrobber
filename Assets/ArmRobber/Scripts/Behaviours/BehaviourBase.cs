﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;

public abstract class BehaviourBase : MonoBehaviourPunCallbacks ,IArmRobberObj<BehaviourBase>
{
    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    public MRobberPlayer PlayerModel { get; protected set; }
    public PhotonView PhotonView { get; protected set; }
    public BehaviourBase GetObject { get => this; set { return; } }

    [Header("Common Network settings")]
    [SerializeField] protected bool runningOnlyOnServer = false;

    public virtual void Initialize(RobotModel model) 
    {
        PlayerModel = model.ArmRobberPlayer;

        if (!PlayerModel.IsLocal) 
        {
            Debug.Log("External copy from : " + PlayerModel.Name);
            return;
        }

        // Enable Default map so the player can play
        InputManager.Instance.EnableMap(PlayerModel.RewiredPlayer.id,true,"Default");
    }

    protected virtual void Awake()
    {
        if (PhotonView == null)
            PhotonView = GetComponent<PhotonView>();

        //PhotonPeer.RegisterType();

#if UNITY_EDITOR
        if (!GameManager.Instance.Debug)
        {
#endif
            if(runningOnlyOnServer && PhotonNetwork.IsMasterClient)
            {
                GameManager.Instance.OnUpdate += UpdateLoop;
                GameManager.Instance.OnFixedUpdate += FixedUpdateLoop;
            }

            if (PhotonView != null && !PhotonView.IsMine) return;
#if UNITY_EDITOR
        }
#endif
        if(runningOnlyOnServer && PhotonNetwork.OfflineMode)
        {
            GameManager.Instance.OnUpdate += UpdateLoop;
            GameManager.Instance.OnFixedUpdate += FixedUpdateLoop;
        }
        else if (!runningOnlyOnServer)
        {
            GameManager.Instance.OnUpdate += UpdateLoop;
            GameManager.Instance.OnFixedUpdate += FixedUpdateLoop;
        }
    }

    protected virtual void OnDestroy()
    {
        if (!GameManager.IsInstanced()) return;
        if (!runningOnlyOnServer)
        {
            if (PhotonView != null && !PhotonView.IsMine) return;
        }

        if (GameManager.Instance.OnUpdate != null) 
            GameManager.Instance.OnUpdate -= UpdateLoop;
        if (GameManager.Instance.OnFixedUpdate != null) 
            GameManager.Instance.OnFixedUpdate-= FixedUpdateLoop;
    }
    
    [PunRPC]
    protected virtual void DestroyObject()
    {
        Destroy(gameObject);
    }

    /*protected virtual void SerializeData(PhotonStream stream, PhotonMessageInfo info)
    {
        if (!photonView.IsMine) return;

        stream.SendNext(MRobberPlayer.Serialize(PlayerModel));
    }
    
    protected virtual void DeserializeData(PhotonStream stream, PhotonMessageInfo info)
    {
        PlayerModel = (MRobberPlayer)MRobberPlayer.Deserialize((byte[])stream.ReceiveNext());

        if (PlayerModel == null) 
        {
            Debug.LogError("We can't deserialize the model");
            return;
        }

        PlayerManager.Instance.FinalizePlayerCreation(gameObject,PlayerModel);
    }
    */
    protected abstract void FixedUpdateLoop();
    protected abstract void UpdateLoop();
    protected abstract void PauseUpdateLoop();

   
}