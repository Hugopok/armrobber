﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubPanel : BasePanel
{
    #region Properties

    public string PanelName => _panelName;
    public BasePanel PreviousPanel => _previousPanel;

    #endregion

    #region Inspector_Fields

    [Space(10)]
    [Header("Sub panel properties")]

    [SerializeField]
    private string _panelName;
    [SerializeField]
    private BasePanel _previousPanel;

    #endregion
    
    public virtual void SetPreviousPanel(BasePanel previousPanel)
    {
        _previousPanel = previousPanel;
    }

    protected override void OpenAnimation()
    {
        base.OpenAnimation();
    }

    protected override void CloseAnimation()
    {
        base.CloseAnimation();
    }
}
