﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public abstract class BasePanel : MonoBehaviour
{
    public bool OpenAtStart => _openAtStart;

    [Header("Core Settings")]
    [Space(10)]
    [SerializeField]
    private bool _openAtStart;
    [SerializeField]
    protected List<SubPanel> subPanels = new List<SubPanel>();

    [Space(10)]
    [Header("Animation Settings")]

    [SerializeField]
    protected Ease easeFunction;
    [SerializeField]
    protected float animationTime;

    [Space(10)]
    [Header("Selection Settings")]

    [SerializeField]
    protected GameObject objectToSelectAtOpening;

    #region Hidden_Fields

    public bool IsOpen
    {
        get { return _isOpen; }
    }
    public bool IsPanelAnimating { get; protected set; }

    public Action<BasePanel> OnClosed;
    public Action<BasePanel> OnOpened;

    public GameObject DefaultSelectedObject => objectToSelectAtOpening;

    protected EventSystem currentEventSystem;

    protected Vector3 startPosition;
    protected RectTransform rectTransform;

    private bool _isOpen;

    #endregion

    protected virtual void Start()
    {
        rectTransform = GetComponent<RectTransform>();

        startPosition = rectTransform.anchoredPosition3D;

        currentEventSystem = EventSystem.current;

        UIManager.Instance.AddPanel(this);

        OpenClose(_openAtStart);
    }

    protected virtual void OnDestroy()
    {
        OnOpened = null;
        OnClosed = null;
    }

    public virtual void OpenClose(bool active)
    {
        _isOpen = active;

        if (!active) OnClose();
        else OnOpen();
    }

    public virtual SubPanel GetSubPanel(string panelName)
    {
        if (subPanels.Count == 0 || string.IsNullOrEmpty(panelName)) return null;

        var sPanel = subPanels.Find(s => s.PanelName == panelName);

        return sPanel;
    }

    public virtual SubPanel GetSubPanel<T>() where T : SubPanel
    {
        if (subPanels.Count == 0) return null;

        var sPanel = subPanels.Find(s => s.GetType() == typeof(T));

        return (T)sPanel;
    }

    protected virtual void OnOpen()
    {
        // Enable the gameobject
        gameObject.SetActive(true);

        OpenAnimation();
    }

    protected virtual void OnClose()
    {
        if (currentEventSystem != null)
            currentEventSystem.SetSelectedGameObject(null);

        CloseAnimation();
    }

    protected virtual void CloseAnimation()
    {
        IsPanelAnimating = true;

        transform.DOScale(0, animationTime).SetEase(easeFunction).SetUpdate(true).OnComplete(() =>
         {
             if (!IsOpen)
                 gameObject.SetActive(false); // Disable the object when the animation end

            IsPanelAnimating = false;

             OnClosed?.Invoke(this);
         });
    }

    protected virtual void OpenAnimation()
    {
        IsPanelAnimating = true;

        transform.DOScale(1, animationTime).SetEase(easeFunction).SetUpdate(true).OnComplete(() =>
        {
            SelectObject();

            IsPanelAnimating = false;

            OnOpened?.Invoke(this);
        });
    }

    protected virtual void SelectObject()
    {
        // Select gameobject when the animation complete to avoid some problems during internal animations
        if (currentEventSystem != null)
            currentEventSystem.SetSelectedGameObject(objectToSelectAtOpening);
    }
}
