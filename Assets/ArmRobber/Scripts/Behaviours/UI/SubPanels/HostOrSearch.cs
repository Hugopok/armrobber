﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostOrSearch : SubPanel
{
    public void SearchHost()
    {
        var hostPanel = GetSubPanel("OnlinePanel") as OnlinePanel;

        UIManager.Instance.OpenNextSubPanel("OnlinePanel");

        //hostPanel.SetState(OnlinePanelState.SearchingHost);
    }

    public void HostMatch()
    {
        LobbyManager.Instance.CreateDefaultRoom();

        UIManager.Instance.OpenPanel<ButtonContainerPanel>();
        //hostPanel.SetState(OnlinePanelState.Hosting);
    }
}
