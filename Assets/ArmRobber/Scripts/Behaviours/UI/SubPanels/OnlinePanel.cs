﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum OnlinePanelState
{
    Hosting,
    ConnectToHosting,
    SearchingHost
}

public class OnlinePanel : SubPanel
{
    [SerializeField]
    private Transform _content;

    [SerializeField]
    private GameObject _scrollItem;

    private Dictionary<string, MatchItem> _items = new Dictionary<string, MatchItem>();

    private void Awake()
    {
        LobbyManager.Instance.OnRoomsUpdated += CreateListItem;
    }

    private void CreateListItem(List<Photon.Realtime.RoomInfo> roomInfos)
    {
        foreach (var room in roomInfos)
        {
#if UNITY_EDITOR
            Debug.Log($" Room updated | Players : {room.PlayerCount} | Name : {room.Name}");
#endif
            if (room == null) continue;

            MatchItem mItem = null;

            if (!_items.ContainsKey(room.Name))
            {
                var go = Instantiate(_scrollItem, _content);
                mItem = go.GetComponent<MatchItem>();
                _items.Add(room.Name, mItem);

                if (objectToSelectAtOpening == null)
                {
                    objectToSelectAtOpening = go;
                    SelectObject();
                }
            }
            else
                mItem = _items[room.Name];

            mItem?.Init(room.Name,
                        room.PlayerCount,
                        room.MaxPlayers,
                        room.IsOpen,
                        room);

        }
    }


    IEnumerator WaitOpen()
    {
        while (IsPanelAnimating)
        {
            yield return null;
        }

        CreateListItem(LobbyManager.Instance.Rooms);
    }

    protected override void OnOpen()
    {
        base.OnOpen();

        StartCoroutine(WaitOpen());
    }

    protected override void OnClose()
    {
        for (int i = 0; i < _content.childCount; i++)
        {
            Destroy(_content.GetChild(i).gameObject);
        }

        base.OnClose();
    }
}
