﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnlineOrLocal : SubPanel
{
    [Space(10)]
    [Header("Sub panels")]

    [SerializeField]
    private string _onlinePanel;
    [SerializeField]
    private string _localPanel;

    public void PlayOnline()
    {

    }

    public void PlayLocal()
    {
        var localpanel = GetSubPanel(_localPanel);

        OpenClose(false);

        localpanel?.OpenClose(true);
    }
}
