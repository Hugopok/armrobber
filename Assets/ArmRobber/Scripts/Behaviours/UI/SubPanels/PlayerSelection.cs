﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelection : SubPanel
{
    [SerializeField]
    private Canvas _mainCanvas;

    [SerializeField]
    private GameObject _panelPrefab;

    private Player _currentPlayer;

    private List<Player> _playersJoinedList = new List<Player>();

    private void CreateGrid()
    {
        if (transform.childCount > 0) return;

        var grid = GetComponent<GridLayoutGroup>();

        grid.cellSize = new Vector2(_mainCanvas.pixelRect.width / InputManager.Instance.MaxPlayersCapacity,
                                    _mainCanvas.pixelRect.height / 2);

        // 8 max player
        for (int i = 0; i < InputManager.Instance.MaxPlayersCapacity; i++)
        {
            var go = Instantiate(_panelPrefab, transform);
        }
    }
}
