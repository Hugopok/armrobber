﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountdownPanel : MonoBehaviour
{
    public TextMeshProUGUI tmPro;

    private void Awake()
    {
        tmPro.SetText(string.Empty);

        ArmNetworkManager.Instance.OnCountDown += Updating;

        ArmNetworkManager.Instance.OnCountDownFinished += Disable;

    }

    private void Updating(string t)
    {
        tmPro.SetText(t);
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        if (!ArmNetworkManager.IsInstanced()) return;

        ArmNetworkManager.Instance.OnCountDown -= Updating;
        ArmNetworkManager.Instance.OnCountDownFinished -= Disable;
    }
}
