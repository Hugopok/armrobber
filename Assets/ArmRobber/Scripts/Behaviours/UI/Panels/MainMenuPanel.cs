﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MainMenuPanel : BasePanel
{
    [SerializeField]
    private string _localOrMultiPanel;

    public void Play()
    {
        OpenClose(false);

        GetSubPanel(_localOrMultiPanel)?.OpenClose(true);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
