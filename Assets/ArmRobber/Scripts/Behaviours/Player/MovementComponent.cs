﻿using Photon.Pun;
using Rewired.Utils.Classes.Data;
using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class MovementComponent : BehaviourBase, IObservable, IPunObservable
{
    #region Properties

    public Vector3 MovementVector => movementVector;

    public float MovementSpeed
    {
        get
        {
            // Se ha preso il powerUp non calcolare la velocità
            if (_isSpeedUp) return _currentSpeed;

            if (_damagableComponent.Health < 0) return _currentSpeed;

            if (_damagableComponent.Health == 0) return _currentSpeed = _speedsByNumArm[0] * 1.5f;

            return _currentSpeed = _speedsByNumArm[(int)_damagableComponent.Health - 1];
        }
    }
    #endregion

    [Space(10)]
    [Header("Movement Component Settings")]
    public Transform RotatingPart;
    public Transform RotatingObjectWithMovement;
    [SerializeField] [Tooltip("MAX 4: Every speed is correlated to the total arms mounted on the robot")] private float[] _speedsByNumArm = new float[4];

    /**
     * Questo vettore rappresenta la direzione di navigazione desiderata.
     */
    private Vector2 inputVector = Vector2.zero;

    /**
     * Questo vettore è la velocità applicata al GameObject ad ogni frame.
     * Include Gravità e MovementSpeed, ed è basata su inputVector.
     */
    private Vector3 movementVector = Vector3.zero;

    [SerializeField][Sirenix.OdinInspector.ReadOnly][HideInWindow]
    private float _currentSpeed;
    private bool _isSpeedUp;

    private CharacterController _controller;
    private DamagableComponent _damagableComponent;

    private void Start()
    {
        StartListening();
        _controller = GetComponent<CharacterController>();
        _damagableComponent = GetComponent<DamagableComponent>();
    }

    public override void Initialize(RobotModel model)
    {
        base.Initialize(model);
    }

    protected override void PauseUpdateLoop() { }

    protected override void UpdateLoop()
    {
        if (PlayerModel == null || _controller == null) return;

        if (!PlayerModel.IsLocal) return;

        GetInputs();

        Vector2 rv = Vector2.zero;

        if (InputManager.Instance.IsPlayerUsingKeyboard(PlayerModel.RewiredPlayer.id))
        {
            rv = InputManager.Instance.GetMousePosition(PlayerModel.RewiredPlayer.id);
            RotateMouse(rv);
        }
        else
        {
            rv = InputManager.Instance.GetRotationVectorRaw(PlayerModel.RewiredPlayer.id);
            RotateController(rv);
        }

        movementVector.x = inputVector.x;
        movementVector.z = inputVector.y;

        movementVector = movementVector.normalized * MovementSpeed;

        Move(movementVector);
    }

    private void GetInputs()
    {
        inputVector = InputManager.Instance.GetInputVectorRaw(PlayerModel.RewiredPlayer.id);
    }

    public void Move(Vector3 mV)
    {
        if (RotatingObjectWithMovement != null)
            RotatingObjectWithMovement?.transform.LookAt(mV, Vector3.up);

        _controller?.SimpleMove(mV);
    }

    public void RotateController(Vector2 rv)
    {
        float inputRot = Mathf.Atan2(rv.x, rv.y);

        RotatingPart.eulerAngles = Vector3.up * inputRot * Mathf.Rad2Deg;
    }

    private void RotateMouse(Vector2 rv)
    {
        // https://wiki.unity3d.com/index.php/LookAtMouse
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position

        Ray ray = Camera.main.ScreenPointToRay(rv);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out float hitDist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitDist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Rotate towards the target point.
            RotatingPart.rotation = targetRotation;
        }
    }

    protected override void FixedUpdateLoop() { }

    public void StartListening()
    {
        ObserverEventManager.AddObserver(ObserverEventNames.POWER_UP_SPEED_EVENT, this);
        ObserverEventManager.AddObserver(ObserverEventNames.DEATH_EVENT, this);
    }

    public void StopListening()
    {
        ObserverEventManager.RemoveObserver(ObserverEventNames.POWER_UP_SPEED_EVENT, this);
        ObserverEventManager.RemoveObserver(ObserverEventNames.DEATH_EVENT, this);
    }

    public void OnEvent(string eventName, object objectPassed)
    {
        if (eventName.Equals(ObserverEventNames.POWER_UP_SPEED_EVENT))
            SpeedUp(objectPassed);
        else if (eventName.Equals(ObserverEventNames.DEATH_EVENT))
            Death(objectPassed);
    }

    protected virtual void SpeedUp(object objectPassed)
    {
        PowerUpEventData<Tuple<float, GameObject>> pData = (PowerUpEventData<Tuple<float, GameObject>>)objectPassed;

        if (pData.Data.Item2 != gameObject) return;

        _isSpeedUp = true;

        _currentSpeed = pData.Data.Item1;

        StartCoroutine(ResetSpeedUp(pData.TimeDuration));
    }

    protected virtual void Death(object objectPassed)
    {
        var pComponent = (PlayerComponent)objectPassed;

        if (pComponent.gameObject != gameObject) return;

        _currentSpeed = 0;
    }

    protected IEnumerator ResetSpeedUp(float duration)
    {
        yield return new WaitForSeconds(duration);

        _isSpeedUp = false;
    }

    protected override void OnDestroy()
    {
        StopListening();

        base.OnDestroy();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //if (stream.IsReading)
        //{
        //    movementVector = (Vector3)stream.ReceiveNext();
        //}
        //else
        //{
        //    stream.SendNext(movementVector);
        //}
    }
}
