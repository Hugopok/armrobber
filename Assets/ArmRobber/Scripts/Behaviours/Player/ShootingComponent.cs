﻿using DG.Tweening;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingComponent : BehaviourBase, IObservable
{
    #region Properties
    public DamagableComponent DamagableComponent 
    {
        get
        {
            if (_robotDamagable == null)
                _robotDamagable = GetComponent<DamagableComponent>();

            return _robotDamagable;
        }
    }

    public List<ArmLinker> GetLinkers => _armLinkers;

    public int GetArmsCount
    {
        get
        {
            if (_armLinkers == null || _armLinkers.Count == 0) return 0;

            return _armLinkers.FindAll(a => a.armWeapon != null).Count;
        }
    }

    [SerializeField][Sirenix.OdinInspector.ShowInInspector]
    public int GetMaxArms => _armLinkers.Count;

    #endregion

    #region Inspector_Fields

    [Header("Shooting Settings")]
    [SerializeField] [HideInWindow] private List<ArmLinker> _armLinkers = new List<ArmLinker>();
    [SerializeField] [HideInWindow] private MovementComponent _movementComponent;
    #endregion

    private DamagableComponent _robotDamagable;
    private BoxCollider _bCollider;

    protected override void Awake()
    {
        base.Awake();

        if (SManager.Instance.Scene == SceneName.MainMenu) return;
        
        Debug.Log("Awake shooting component ");

        if (_movementComponent == null)
            _movementComponent = GetComponent<MovementComponent>();

        _robotDamagable = GetComponent<DamagableComponent>();

        _bCollider = GetComponent<BoxCollider>();

        StartListening();

        if (_robotDamagable != null)
            _robotDamagable.OnDamage.AddListener(ListenDamageEvent);

        ObserverEventManager.Trigger<ShootingComponent>(ObserverEventNames.SHOOTING_COMPONENT_INITIALIZED, this);
    }

    protected override void PauseUpdateLoop() { }

    protected override void UpdateLoop()
    {
        if (!PlayerModel.IsLocal) return;

        GetInput();
    }

    protected override void FixedUpdateLoop() { }

    public virtual void PrepareForBattle()
    {
        if(_robotDamagable == null)
            _robotDamagable = GetComponent<DamagableComponent>();

        for (int i = GetMaxArms; i > SettingsManager.Instance.GameplaySettings.StartWithWeapons; i--)
        {
            var linker = GetLinkerToRemove();

            if (linker == null || linker.armWeapon == null) continue;

            Destroy(linker.armWeapon.gameObject);

            linker.RemoveWeapon();
        }

        _robotDamagable.Health = GetArmsCount;
    }

    public virtual void CmdFire(ArmWeapon weapon)
    {
        if (!weapon.CanShoot) return;

        ObserverEventManager.Trigger<ArmWeapon>(ObserverEventNames.SHOOTING_EVENT,weapon);

        weapon.photonView.RPC("Shoot",RpcTarget.MasterClient,photonView.ViewID);

        PhotonNetwork.SendAllOutgoingCommands();
    }

    [PunRPC][Sirenix.OdinInspector.Button]
    public virtual void UnmountArm()
    {
        // Prendi il primo linker disponibile da rimuovere
        // il primo linker disponibile sarà quello con un'arma attaccata
        ArmLinker linker = GetLinkerToRemove();

        if (linker == null)
        {
#if UNITY_EDITOR
            Debug.LogWarning("Linker is null");
#endif
            return;
        }
        if (linker.armWeapon.IsPopping)
        {
#if UNITY_EDITOR
            Debug.LogError("weapon is already popping");
#endif
            linker.RemoveWeapon();
            // disattiva il linker
            linker.linker.gameObject.SetActive(false);
            return;
        }

        // Smonta l'arma dal braccio
        linker.armWeapon.Unmount(_movementComponent.MovementVector);

        // rimuovi la referenza dell'arma dal braccio
        linker.RemoveWeapon();

        // disattiva il linker
        linker.linker.gameObject.SetActive(false);

        RefreshCollider();
    }

    public virtual void MountArm(ArmWeapon weapon)
    {
        if (weapon.IsMounted) return;

        // prendi il primo linker disponibile
        var linker = GetNextLinkerAvaiable();

        if (linker == null) return;

        // settalo come visibile
        linker.linker.gameObject.SetActive(true);

        // setta la referenza
        linker.SetWeapon(weapon);

        // Monta l'arma sul linker
        weapon.Mount(this, linker);

        if(PhotonNetwork.IsMasterClient)
            ReloadSameType(weapon,true);

        RefreshCollider();

        // Aggiungi un punto vita al robot
        _robotDamagable.AddHealth(1);
    }

    protected virtual void ReloadSameType(ArmWeapon weapontype,bool reloadInstant)
    {
        var sameWeapons = _armLinkers.FindAll(l => l?.armWeapon?.name == weapontype.name);

        if (sameWeapons.Count <= 1) return;

        foreach (var linker in sameWeapons)
        {
            if (linker == null || linker.armWeapon == null) continue;

            if (linker.armWeapon.name != weapontype.name) continue;

            if (reloadInstant)
                linker.armWeapon.photonView.RPC("ReloadInstant", RpcTarget.MasterClient);
            else
                linker.armWeapon.photonView.RPC("Reload", RpcTarget.MasterClient);
        }
    }

    [PunRPC]
    public virtual void ArmPicked(int photonViewId)
    {
        GameObject objectPicked = PhotonView.Find(photonViewId).gameObject;

        var weapon = objectPicked.GetComponent<ArmWeapon>();

        if (weapon.IsPopping) return;

        Debug.Log($"{objectPicked.name} Photon Id : {weapon.photonView.ViewID}");

        if (weapon == null) return;

        if (weapon.IsMounted) return;

        var pickable = objectPicked.GetComponent<ArmWeaponPickable>();

        pickable.PickedEffect();

        MountArm(weapon);
    }

    public void StartListening()
    {
        ObserverEventManager.AddObserver(ObserverEventNames.WEAPON_GRABBED_EVENT,this);
        ObserverEventManager.AddObserver(ObserverEventNames.WEAPON_DESTROY + gameObject.name, this);
    }

    public void StopListening()
    {
        ObserverEventManager.RemoveObserver(ObserverEventNames.WEAPON_GRABBED_EVENT, this);
        ObserverEventManager.RemoveObserver(ObserverEventNames.WEAPON_DESTROY + gameObject.name, this);
    }

    public void OnEvent(string eventName, object objectPassed)
    {
        if (eventName.Equals(ObserverEventNames.WEAPON_GRABBED_EVENT))
        {
            PowerUpEventData<Tuple<GameObject, GameObject>> data =
            (PowerUpEventData<Tuple<GameObject, GameObject>>)objectPassed;

            // Se non è lui che ha grabbato l'oggetto, return
            if (data.Data.Item2 != gameObject) return;

            var view = data.Data.Item1.GetComponent<PhotonView>();

            photonView.RPC("ArmPicked", RpcTarget.AllViaServer, view.ViewID);

            PhotonNetwork.SendAllOutgoingCommands();
        }
        else if (eventName.Equals(ObserverEventNames.WEAPON_DESTROY + gameObject.name))
        {
            ArmWeapon weapon = (ArmWeapon)objectPassed;

            var linker = _armLinkers.Find(l => l.armWeapon == weapon);

            linker.RemoveWeapon();

            linker.linker.gameObject.SetActive(false);

            _robotDamagable.DamageWithNoEffects(1);

            // NON so se funziona al 90 % no
            if (weapon.photonView.IsMine)
                PhotonNetwork.Destroy(weapon.photonView);
        }
    }

    protected virtual void ListenDamageEvent(DamagableComponent damagable)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("UnmountArm", RpcTarget.All);

            PhotonNetwork.SendAllOutgoingCommands();
        }
    }

    protected virtual void GetInput()
    {
        if (!InputManager.IsInstanced())
            return;

        if (InputManager.Instance.IsFireButtonHold(PlayerModel.RewiredPlayer.id))
        {
            foreach (ArmLinker linker in _armLinkers)
            {
                if (linker == null) continue;

                if (linker.armWeapon == null) continue;

                CmdFire(linker.armWeapon);
            }
        }

        if (InputManager.Instance.IsReloadButtonPressed(PlayerModel.RewiredPlayer.id))
        {
            foreach (ArmLinker linker in _armLinkers)
            {
                if (linker == null) continue;

                if (linker.armWeapon == null) continue;

                linker.armWeapon.photonView.RPC("Reload",RpcTarget.MasterClient);
            }
        }
    }

    protected ArmLinker GetNextLinkerAvaiable()
    {
        for (int i = 0; i < _armLinkers.Count; i++)
        {
            var linker = _armLinkers[i];

            if (linker.armWeapon == null) return linker;
        }

        return null;
    }

    protected ArmLinker GetLinkerToRemove()
    {
        for (int i = _armLinkers.Count - 1; i >= 0; i--)
        {
            var linker = _armLinkers[i];

            if (linker.armWeapon != null) return linker;
        }

        return null;
    }

    public virtual void RefreshCollider()
    {
        Bounds bounds = new Bounds(transform.position, Vector3.zero);

        var allDescendants = gameObject.GetComponentsInChildren<Transform>();

        foreach (Transform desc in allDescendants)
        {
            Renderer childRenderer = desc.GetComponent<Renderer>();

            if (desc.GetComponent<ParticleSystem>()) continue;

            if (childRenderer != null)
            {
                bounds.Encapsulate(childRenderer.bounds);
            }

            _bCollider.center = Vector3.zero;
            _bCollider.size = new Vector3(bounds.size.x, 5, bounds.size.z);
        }
    }

    protected override void OnDestroy()
    {
        StopListening();

        base.OnDestroy();
    }
}
