﻿using Photon.Pun;
using UnityEngine;

/// <summary>
/// Player component è una classe che rappresenta lo stato globale del player,
/// è un BehaviourBase (MonoBehaviour) e contiene uno shortcut a tutti gli script che ha il gameobject
/// e lo stato per ora solo se il giocatore è morto si può vedere
/// </summary>
public class PlayerComponent : BehaviourBase
{
    public DamagableComponent DamagableComponent => damagableComponent;
    public ShootingComponent ShootingComponent => shootingComponent;
    public MovementComponent MovementComponent => movementComponent;
    public bool IsPlayerDead { get; private set; }

    [SerializeField] [HideInWindow]protected DamagableComponent damagableComponent;
    [SerializeField] [HideInWindow]protected ShootingComponent shootingComponent;
    [SerializeField] [HideInWindow]protected MovementComponent movementComponent;

    

    protected override void Awake()
    {
        base.Awake();
 
        damagableComponent.OnDeath.AddListener(PlayerDeath);
    }

    protected override void FixedUpdateLoop() { }

    protected override void PauseUpdateLoop() { }

    protected override void UpdateLoop() { }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        damagableComponent?.OnDeath?.RemoveListener(PlayerDeath);
    }

    [PunRPC]
    protected void RPC_SendModel(object byteArray)
    {
        if (PlayerModel != null) return;

        var model = MRobberPlayer.Deserialize((byte[])byteArray);

        PlayerManager.Instance.FinalizePlayerCreation(gameObject, (MRobberPlayer)model);
    }

    private void PlayerDeath(DamagableComponent damagableComponent)
    {
        IsPlayerDead = true;
        
        ObserverEventManager.Trigger<PlayerComponent>(ObserverEventNames.DEATH_EVENT, this);
    }
}
