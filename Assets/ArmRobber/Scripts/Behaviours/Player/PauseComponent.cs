﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseComponent : BehaviourBase
{
    protected override void FixedUpdateLoop() { }

    protected override void PauseUpdateLoop()
    {

    }

    protected override void UpdateLoop()
    {
        if (PlayerModel == null || !PlayerModel.IsLocal) return;

        if (InputManager.Instance.IsPauseButtonPressed(PlayerModel.RewiredPlayer.id))
        {
            GameManager.Instance.State = GameState.Pause;
        }
    }
}
