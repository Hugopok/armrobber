﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using DG.Tweening;
using Photon.Realtime;

[Serializable]
public class ArmWeapon : MonoBehaviourPunCallbacks, IPunObservable, IPunOwnershipCallbacks
{
    #region Properties

    public bool CanShoot
    {
        get
        {
            if (PhotonNetwork.IsMasterClient)
                return (Time.time >= NextTimeToFire);
            else 
                return PhotonNetwork.Time >= NextTimeToFire;
        }
    }
    public float FireRate => _fireRate;
    public int MaxAmmos => _maxAmmos;
    public int MaxAmmoPerMag => _maxAmmoPerMag;
    public float ReloadTime => _reloadTime;
    //public float BulletSpeed => _bulletSpeed;
    public bool DestroyWhenNoAmmo => _destroyWhenNoAmmo;
    //public bool DestroyBulletOnHit => _destroyBulletOnHit;
    public float NextTimeToFire => _nextTimeToFire;
    public bool IsMounted => transform.parent != null;
    public bool IsPopping => _isPopping;
    public int CurrentAmmo => _currentAmmo;
    public ShootingComponent ShootingComponent => _shootingComponent;
    #endregion

    public PhotonView PhotonView => _photonView;
    
    [Header("Important")]
    [SerializeField]
    private PhotonView _photonView;

    [Header("Shooting settings")]
    [SerializeField]
    private Transform _projectileSpawn;
    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private GameObject _muzzleParticle;

    [Header("Shooting tuning Settings")]
    [SerializeField]
    private float _fireRate = 1;
    [SerializeField]
    private int _maxAmmos;
    [SerializeField]
    private int _maxAmmoPerMag;
    [SerializeField]
    private float _reloadTime;
    [SerializeField]
    private bool _destroyWhenNoAmmo;
    [SerializeField]
    private bool _destroyBulletOnHit;
    [SerializeField]
    private bool _muzzleWhenShoot;
    [SerializeField]
    private string _bulletPath;

    [Header("Popping settings")]
    [SerializeField] private float _popSpeed = 3;
    public int minTime = 50;
    public int resolutionCurve = 25;
    public float popVelocity = 2000;

    [Header("Feedbacks")]
    [SerializeField] private ShootFeedback _sFeedback;
    [SerializeField] private ReloadFeedback _rFeedback;
    [SerializeField] private PopFeedback _popFeedback;

    [Header("Debug")]
    [SerializeField]
    [ReadOnly][HideInWindow]
    private float _nextTimeToFire;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private int _currentAmmo;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private int _ammoPerMag;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private int _ammoRemains;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private bool _isPopping;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private Rigidbody _rb;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private Vector3 _originalPos;
    [SerializeField]
    [ReadOnly]
    [HideInWindow]
    private ShootingComponent _shootingComponent;
    
    private Coroutine _realoadRoutine;
    private GameObject _owner;
    private WaitForEndOfFrame _wait;
    private ParticleSystem _mParticle;
    private Vector3[] _bezierCurve;
    [SerializeField]
    private int _pathIndex;
    private Tween _rotationTween;
    private Smooth.SmoothSyncPUN2 _smoothSync;

    protected virtual void Awake()
    {
        if (_photonView == null)
            _photonView = GetComponent<PhotonView>();

        _smoothSync = GetComponent<Smooth.SmoothSyncPUN2>();

        PhotonNetwork.AddCallbackTarget(this);

        //try
        //{
        //    if (photonView.IsMine && !PhotonNetwork.IsMasterClient)
        //        photonView.TransferOwnership(PhotonNetwork.MasterClient);
        //}
        //catch (Exception e)
        //{
        //    Debug.Log("Armweapon Ex : " + e.Message);
        //}

        if (_rb == null)
            _rb = GetComponent<Rigidbody>();
        if (_rb == null)
            _rb = gameObject.AddComponent<Rigidbody>();
        if (_sFeedback == null)
            _sFeedback = GetComponent<ShootFeedback>();
        if (_rFeedback == null)
            _rFeedback = GetComponent<ReloadFeedback>();

        _currentAmmo = _maxAmmoPerMag;
        _ammoPerMag = _maxAmmoPerMag;
        _ammoRemains = _maxAmmos;
    }

    private void Update()
    {
        if (!photonView.IsMine) return;

        if (_shootingComponent == null)
        {
            return;
        }

        if (IsMounted)
        {
            if (transform.localPosition.x > 0 ||
                transform.localPosition.y > 0 ||
                transform.localPosition.z > 0)
            {
                Debug.LogError("Non è messo bene, lo metto bene");
                SetToZero();
            }
        }
    }

    private void OnTransformParentChanged()
    {
        if (transform.parent == null)
        {
            _shootingComponent = null;
        }
        else
        {
            SetToZero();

            if (_shootingComponent != null)
                _shootingComponent.RefreshCollider();
        }
    }

    [PunRPC]
    public virtual void Shoot(int ownerId)
    {
        Debug.Log("Shooting");

        if (_realoadRoutine != null) return;

        if (!CanShoot) return;

        if (_currentAmmo <= 0)
        {
            Debug.Log("Can't shoot i must to reload");
            photonView.RPC("Reload", RpcTarget.MasterClient);
            return;
        }

        _owner = PhotonNetwork.GetPhotonView(ownerId).gameObject;

       if(photonView.IsMine)
            _nextTimeToFire = Time.time + (1 / _fireRate);

        if (_muzzleWhenShoot)
        {
            Debug.Log("Muzzle");
            //photonView.RPC("ActivateMuzzle", RpcTarget.All);
        }

        GameObject projectile = null;

        projectile = PhotonNetwork.Instantiate(_bulletPath, _projectileSpawn.position, Quaternion.identity);

        Bullet bullet = projectile.GetComponent<Bullet>();

        bullet.InitializeBullet(photonView.ViewID,ownerId, _projectileSpawn.transform.forward);

        bullet.photonView.RPC("RPC_Initialize", RpcTarget.Others, _projectileSpawn.transform.forward);

        PhotonNetwork.SendAllOutgoingCommands();

        _sFeedback?.Play(gameObject, this);

        _currentAmmo--;
        _ammoRemains--;
    }

    public virtual void Unmount(Vector3 popVector)
    {
        _shootingComponent = null;

        _owner = null;

        StartCoroutine(Wait(() => PopWeapon(popVector),1)); 
    }

    public virtual void Mount(ShootingComponent where, ArmLinker linker)
    {
        _shootingComponent = where;

        _owner = where.gameObject;

        photonView.RPC("PopEffectStopBrutality", RpcTarget.All);

        GrabArm(linker.armPosition);
    }

    [PunRPC]
    protected virtual void ActivateMuzzle()
    {
        if (_muzzleParticle == null) return;

        if (_mParticle == null)
            _mParticle = _muzzleParticle.GetComponent<ParticleSystem>();

        _mParticle.Simulate(0.0f, true, true);
        _mParticle.Play();

        _muzzleParticle.SetActive(true);

        StartCoroutine(WaitSeconds(_mParticle.main.duration, () =>
        {
            _mParticle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
            _muzzleParticle.SetActive(false);
        }));
    }

    protected IEnumerator WaitSeconds(float seconds, Action onComplete)
    {
        yield return new WaitForSeconds(seconds);

        onComplete?.Invoke();
    }

    protected virtual void PopWeapon(Vector3 popVector)
    {
        if (!photonView.IsMine)
        {
            transform.SetParent(null, true);
            _isPopping = true;
            return;
        }

        if (_isPopping) return;

        if (_rb == null) return;

        transform.SetParent(null, true);

        StartCoroutine(WaitTransform(popVector));
    }

    protected IEnumerator WaitTransform(Vector3 popVector)
    {
        yield return new WaitUntil(() => transform.parent == null);

        _isPopping = true;

        _rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        _rb.freezeRotation = false;

        // TODO : COSA IMPORTANTISSIMA MEGAAA Commentare tuttooo 

        var endPosition = PopZoneManager.Instance.GetRandomPointFrom(transform.position);
        var center = BezierUtils.CenterOfVectors(new Vector3[] { transform.position, endPosition });
        center.y = CameraManager.Instance.PopTransformPoint.position.y;

        _bezierCurve = BezierUtils.CreateCurve(resolutionCurve, transform.position,
                                               center,
                                               endPosition);

        _bezierCurve[0] = transform.position;
        _bezierCurve[_bezierCurve.Length - 1] = endPosition;

        int vertexCount = _bezierCurve.Length;

        float flightDuration = 0;

        for (int i = 0; i < vertexCount; i++)
            flightDuration += GetTByQuadraticPoint(i, _bezierCurve.Length) / popVelocity;

        _rotationTween = transform.DORotate(new Vector3(360, 0, 360), flightDuration, RotateMode.FastBeyond360).
                  //SetLoops(-1, LoopType).
                  SetRelative(true).
                  SetEase(Ease.Linear);



        for (int i = 0; i < vertexCount; i++)
        {
            float time = GetTByQuadraticPoint(i, _bezierCurve.Length) / popVelocity;

            transform.DOMove(_bezierCurve[i], time, false).SetEase(Ease.Linear);

            if (i == 0)
            {
                photonView.RPC("PopEffectStart", RpcTarget.All);
            }
            else if (i == vertexCount - 1)
                photonView.RPC("PopEffectEnd", RpcTarget.All);

            yield return new WaitForSeconds(time);
        }

        _isPopping = false;

        _rotationTween.Kill(false);

        //TODO : Provare ad aggiungere lo shake 

    }


    private void OnDrawGizmos()
    {
        if (_bezierCurve == null || _bezierCurve.Length == 0) return;

        foreach (var item in _bezierCurve)
        {
            Gizmos.color = Color.cyan;

            Gizmos.DrawSphere(item,0.5f);
        }
    }

    private float GetTByQuadraticPoint(int x,int vertexCount)
    {
        // ax^2 + bx + c
        // c = vertexCount
        // b = ??
        return Mathf.Max(-Mathf.Pow(x,2) + (vertexCount - 1)*x, minTime);
    }

    protected IEnumerator Wait(Action onWaited, int frameToWait = 1)
    {
        for (int i = 0; i < frameToWait; i++)
            yield return _wait;

        onWaited?.Invoke();
    }

    [PunRPC]
    public void PopEffectStart()
    {
        _popFeedback.TrailEffectDuration = -1f;
        _popFeedback?.Play(gameObject, false);
    }

    [PunRPC]
    public void PopEffectEnd()
    {
        _popFeedback?.Play(gameObject, true);
    }

    [PunRPC]
    public void PopEffectStopBrutality()
    {
        _popFeedback.BruteForceStop = true;
    }

    [PunRPC]
    public void ReloadInstant()
    {
        if (_realoadRoutine != null)
        {
            StopCoroutine(_realoadRoutine);
            _realoadRoutine = null;
        }
        _currentAmmo = AmmoToReload();
    }

    [PunRPC]
    public void Reload()
    {
        if (_ammoRemains <= 0 && _destroyWhenNoAmmo)
        {
            ObserverEventManager.Trigger(ObserverEventNames.WEAPON_DESTROY + _owner.name, this);

            return;
        }

        if (_realoadRoutine == null)
        {
            _realoadRoutine = StartCoroutine(Reload(ReloadTime));
        }
    }

    protected IEnumerator Reload(float time)
    {
        // TODO : Feedback system 

        yield return new WaitForSeconds(time);

        _currentAmmo = AmmoToReload();

        _rFeedback?.Play(gameObject, this);

        if (_realoadRoutine != null)
        {
            StopCoroutine(_realoadRoutine);
            _realoadRoutine = null;
        }
    }

    protected int AmmoToReload()
    {
        // Infinite ammos
        if (_maxAmmos <= -1)
            return _ammoPerMag;

        int bulletsToLoad = _ammoPerMag - _currentAmmo;

        return (_ammoRemains >= bulletsToLoad) ? bulletsToLoad : _ammoRemains;
    }

    protected virtual void GrabArm(Transform parent)
    {
        transform.SetParent(parent, true);

        transform.localScale = Vector3.one;

        _smoothSync.teleportAnyObject(Vector3.zero, Quaternion.identity, Vector3.one);

        if (_rb == null) return;

        _rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

        _rb.isKinematic = true;
    }

    protected virtual void SetToZero()
    {
        transform.localPosition = Vector3.zero;

        transform.rotation = Quaternion.Euler(Vector3.zero);

        transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsReading)
        {
            _isPopping = (bool)stream.ReceiveNext();
            
            _nextTimeToFire = (float)stream.ReceiveNext();
            _ammoPerMag =  (int)stream.ReceiveNext();
            _ammoRemains = (int)stream.ReceiveNext();
            _currentAmmo = (int)stream.ReceiveNext();
        }
        else
        {
            stream.SendNext(_isPopping);

            stream.SendNext(_nextTimeToFire);
            stream.SendNext(_ammoPerMag);
            stream.SendNext(_ammoRemains);
            stream.SendNext(_currentAmmo);
        }
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {

    }
    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        var smooth = GetComponent<Smooth.SmoothSyncPUN2>();
        
        smooth.clearBuffer();
    }

    private void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    
}