﻿using System.Linq;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;

public abstract class Bullet : BehaviourBase
{
    public bool IsStopped { get; protected set; }

    [SerializeField] protected float radiusSphereCast;

    protected bool initialized;
    protected Vector3 prevPos;
    protected Vector3 spawnPosition;
    protected int weaponId;
    protected bool isObjectHitted;

    [Header("Bullet Settings")]
    public LayerMask ObstacleLayer;
    public bool DestroyBulletOnHit;
    public float DamageToInflict;
    [SerializeField]
    protected float bulletSpeed;

    [Header("Debug")]

    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    [HideInWindow]
    protected Vector3 bulletDirection;
    [SerializeField]
    [HideInWindow]
    [Sirenix.OdinInspector.ReadOnly]
    protected int ownerId;
    [SerializeField]
    [Sirenix.OdinInspector.ReadOnly]
    [HideInWindow]
    protected Collider[] collidersHitted = new Collider[10];
   
    public virtual void InitializeBullet(int weaponId ,int ownerId, Vector3 dir)
    {
        spawnPosition = transform.position;
        this.weaponId = weaponId;
        bulletDirection = dir;
        initialized = true;
        this.ownerId = ownerId;

        GetComponent<Smooth.SmoothSyncPUN2>().teleportAnyObject(transform.position, Quaternion.identity, transform.localScale);

        StartCoroutine(DestroyAfterSeconds());
        //Destroy(gameObject,10);
    }

    private IEnumerator DestroyAfterSeconds()
    {
        yield return new WaitForSeconds(10);

        DestroyBullet();
    }

    [PunRPC]
    public virtual void RPC_Initialize(Vector3 dir)
    {
        Debug.Log("Initializing bullet with RPC call");
        bulletDirection = dir;
        initialized = true;
    }

    protected override void Awake()
    {
        if (PhotonView == null)
            PhotonView = GetComponent<PhotonView>();

        Destroy(gameObject, 10);

        GameManager.Instance.OnUpdate += UpdateLoop;

        if (!PhotonNetwork.IsMasterClient) return;
            
        GameManager.Instance.OnFixedUpdate += FixedUpdateLoop;
    }

    protected override void FixedUpdateLoop() => CustomCollider();

    protected override void PauseUpdateLoop() { }

    protected override void UpdateLoop() 
    {
        if (!initialized) return;
        if (IsStopped) return;

        MoveBullet(bulletSpeed, bulletDirection);
    }

    protected virtual void CustomCollider()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (isObjectHitted) return;

        RaycastHit hit;

        collidersHitted.ToList().Clear();

        int count = Physics.OverlapSphereNonAlloc(transform.position, radiusSphereCast,collidersHitted, ObstacleLayer);

        if (count <= 0) return;

        for (int i = 0; i < count; i++)
        {
            var col = collidersHitted[i];

            if (col == null || col.gameObject == null) continue;

            var view = col.gameObject.GetComponent<PhotonView>();

            if (ownerId == view?.ViewID || weaponId == view?.ViewID) continue;

            Vector3 rayStart = col.ClosestPoint(prevPos) - bulletDirection;
            Vector3 rayDirection = bulletDirection;

            // TODO : Fixare bug raycast
            if (Physics.Raycast(rayStart, rayDirection, out hit, 2, ObstacleLayer))
            {
                if (IsOwner(hit.transform.gameObject)) continue;

                if (CheckObjectHitted(hit.transform.gameObject))
                {
                    isObjectHitted = true;

                    ObjectHitted(hit);

                    if (DestroyBulletOnHit)
                    {
                        DestroyBullet();
                    }

                    return;
                }
            }
        }
    }

    protected bool IsOwner(GameObject other)
    {
        var view = other.gameObject.GetComponent<PhotonView>();

        if(view == null)
        {
            return false;
        }

        return ownerId == view.ViewID || view.ViewID == weaponId;
    }

    protected virtual bool CheckObjectHitted(GameObject objectHitted)
    {
        DamagableComponent damagable = objectHitted.GetComponent<DamagableComponent>();

        return damagable != null || LayerUtils.IsInLayerMask(objectHitted.layer, ObstacleLayer);
    }

    protected virtual void MoveBullet(float bulletSpeed,Vector3 dir)
    {
        prevPos = transform.position;

        transform.Translate(dir * bulletSpeed * Time.deltaTime);
    }

    [PunRPC]
    protected override void DestroyObject()
    {
        if (PhotonNetwork.IsMasterClient)
            PhotonNetwork.Destroy(photonView);
        else
            Destroy(gameObject);
    }

    protected virtual void DestroyBullet()
    {
        if (PhotonNetwork.IsMasterClient)
            DestroyObject();
        else
            photonView.RPC("DestroyObject", RpcTarget.Others);
    }

    protected override void OnDestroy()
    {
        StopAllCoroutines();
        PhotonNetwork.RemoveCallbackTarget(this);

        if (!GameManager.IsInstanced()) return;
        
        if (GameManager.Instance.OnUpdate != null) GameManager.Instance.OnUpdate -= UpdateLoop;
        if (GameManager.Instance.OnFixedUpdate != null) GameManager.Instance.OnFixedUpdate -= FixedUpdateLoop;
    }

    protected abstract void ObjectHitted(RaycastHit collision,bool destroyParticles = true);

    protected abstract void ObjectHitted(Collision collision,bool destroyParticles = true);

#if UNITY_EDITOR
    protected virtual void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position,radiusSphereCast);
    }
#endif
}