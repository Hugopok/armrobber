﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using UnityEngine;

public enum DeathConditions
{
    DeathOnZero,
    DeathMinusZero
}

[Serializable]
public class DamagableComponent : MonoBehaviour, IPunObservable
{
    [HideInWindow]
    public DamagableEvent OnDamage = new DamagableEvent();
    [HideInWindow]
    public DamagableEvent OnAddHealth = new DamagableEvent();
    [HideInWindow]
    public DamagableEvent OnDeath = new DamagableEvent();

    public float Health { get => health; set => health = value; }
    public bool IsInvincible => invulnerabilityTime > 0 && isInvulnerable;
    public bool GodMode => godMode;
    public float InvulnerableTime => invulnerabilityTime;

    [Header("Settings")]
    [SerializeField] protected bool godMode;
    [SerializeField] protected float health;
    [SerializeField] protected float maxHealth;
    [SerializeField] protected DeathConditions deathConditions;
    [SerializeField] protected float invulnerabilityTime;
    [SerializeField] protected PhotonView photonView;

    [Space(10)]
    [Header("Feedbacks")]

    [SerializeField] protected AFeedback hitFeedback;
    [SerializeField] protected AFeedback hitForceFeedback;

    [SerializeField][Sirenix.OdinInspector.ReadOnly]
    protected bool isInvulnerable;
    [SerializeField][Sirenix.OdinInspector.ReadOnly]
    protected bool isPerforming;

    protected virtual void Start() 
    {
        if (photonView == null)
            photonView = GetComponent<PhotonView>();

        PhotonNetwork.AddCallbackTarget(this);

        //RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
        //PhotonNetwork.RaiseEvent(PUNEventCodes.DAMAGE_EVENT, content, raiseEventOptions, SendOptions.SendReliable);
    }

    public virtual void Damage(ContactPoint contactPoint, float damage)
    {
        hitForceFeedback?.Play(null,-contactPoint.normal);


        Damage(damage);
        //photonView.RPC("Damage", RpcTarget.MasterClient, damage);
    }
#if UNITY_EDITOR
    [Sirenix.OdinInspector.Button]
    public virtual void DamageTest(float damage = 1)
    {
        photonView.RPC("Damage", RpcTarget.AllViaServer, damage);
    }
#endif
    public virtual void Damage(RaycastHit contactPoint, float damage)
    {
        hitForceFeedback?.Play(null, -contactPoint.normal);

        if(PhotonNetwork.IsMasterClient)
            Damage(damage);
        //photonView.RPC("Damage", RpcTarget.MasterClient, damage);
    }

    public virtual void DamageWithNoEffects(float damage)
    {
        health -= damage;
    }

    [PunRPC]
    protected virtual void Damage(float damage)
    {
        Debug.Log($"Hitted {gameObject.name} : [ Is performing {isPerforming} ] [ God mode {godMode} ] [ Invulnerability {isInvulnerable} ] [ Health {Health}] ",gameObject);

        if (godMode || isInvulnerable)
        {
            Debug.Log("Non posso danneggiare, perchè è invulnerabile");
            return;
        }

        if (deathConditions == DeathConditions.DeathOnZero && health <= 0) return;

        if (isPerforming)
        {
            Debug.Log($"Is already performing {isPerforming} : {gameObject.name}");
            return;
        };

        isPerforming = true;

        if (invulnerabilityTime > 0)
        {
            StartCoroutine(InvulnerabilityTime(invulnerabilityTime));
        }

        photonView.RPC("RPC_Damage",RpcTarget.All,damage);
        PhotonNetwork.SendAllOutgoingCommands();
        if (deathConditions == DeathConditions.DeathMinusZero)
        {
            if (health < 0)
                photonView.RPC("RPC_Death",RpcTarget.All);
            else
                photonView.RPC("RPC_HitFeedback", RpcTarget.All);
        }
        else
        {
            if (health == 0)
                photonView.RPC("RPC_Death", RpcTarget.All);
            else
                photonView.RPC("RPC_HitFeedback",RpcTarget.All);
        }

        
    }

    public virtual void AddHealth(float healthToAdd)
    {
        if (health < maxHealth)
        {
            health += healthToAdd;
            OnAddHealth?.Invoke(this);
        }
    }

    [PunRPC]
    protected virtual void RPC_Death()
    {
        OnDeath?.Invoke(this);
    }

    [PunRPC]
    protected virtual void RPC_Damage(float damage)
    {
        health -= damage;
        OnDamage?.Invoke(this);
        ObserverEventManager.Trigger<DamagableComponent>(ObserverEventNames.DAMAGABLE_EVENT, this);
    }

    [PunRPC]
    protected virtual void RPC_HitFeedback()
    {
       hitFeedback?.Play(gameObject, invulnerabilityTime);
    }

    protected virtual IEnumerator InvulnerabilityTime(float time)
    {
        isInvulnerable = true;

        yield return new WaitForSeconds(time);

        isInvulnerable = false;
        isPerforming = false;
    }

    public void OnEvent(EventData photonEvent)
    {
        throw new NotImplementedException();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsReading)
        {
            godMode = (bool)stream.ReceiveNext();
            health = (float)stream.ReceiveNext();
            maxHealth = (float)stream.ReceiveNext();
            deathConditions = (DeathConditions)(int)stream.ReceiveNext();
            invulnerabilityTime = (float)stream.ReceiveNext();
            isInvulnerable = (bool)stream.ReceiveNext();
        }
        else
        {
            stream.SendNext(godMode);
            stream.SendNext(health);
            stream.SendNext(maxHealth);
            stream.SendNext((int)deathConditions);
            stream.SendNext(invulnerabilityTime);
            stream.SendNext(isInvulnerable);
        }
    }

    private void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
}
