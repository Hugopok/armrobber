﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncingBullet : GenericBulletComponent
{
    [SerializeField]
    private int _maxBounce;
    [SerializeField]
    private int _currentBounce;

    public override void InitializeBullet(int weaponId, int ownerId, Vector3 dir)
    {
        base.InitializeBullet(ownerId,weaponId, dir);
    }

    [PunRPC]
    public override void RPC_Initialize(Vector3 dir)
    {
        base.RPC_Initialize(dir);
    }

#if UNITY_EDITOR
    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        DrawPredictedReflectionPattern(transform.position,bulletDirection,_maxBounce);
    }
#endif

    protected override void CustomCollider()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (isObjectHitted) return;

        RaycastHit hit;

        if(Physics.Raycast(transform.position,
                           bulletDirection,
                           out hit,
                           1))
        {
            if (IsOwner(hit.transform.gameObject)) return;

            int layer = hit.transform.gameObject.layer;

            if (LayerMask.LayerToName(layer) == "Player")
            {
                isObjectHitted = true;

                ObjectHitted(hit);

                DestroyBullet();
            }
            else if (LayerMask.LayerToName(layer) == "Wall" ||
                    LayerMask.LayerToName(layer) == "Shootable")
            {
                bulletDirection = Vector3.Reflect(bulletDirection, hit.normal);

                bulletDirection.y = 0; // Per non far alzare il proiettile se l'angolo è verso l'alto

                _currentBounce++;

                ObjectHitted(hit,false);

                if (_currentBounce >= _maxBounce)
                {
                    DestroyParticles(hit);

                    DestroyBullet();
                }
            }
        }
    }

    [PunRPC]
    protected override void ActiveImpactParticle(Vector3 position, Vector3 normal,bool isRpc = false)
    {
        base.ActiveImpactParticle(position, normal,isRpc);
    }

    [PunRPC]
    protected override void DestroyParticles(RaycastHit contactPoint)
    {
        base.DestroyParticles(contactPoint);
    }

    [PunRPC]
    protected override void DestroyObject()
    {
        base.DestroyObject();
    }

    protected override void OnDestroy()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
        base.OnDestroy();
    }

    private void DrawPredictedReflectionPattern(Vector3 position, Vector3 direction, int reflectionsRemaining)
    {
        if (reflectionsRemaining == 0)
            return;
        
        Vector3 startingPosition = new Vector3(position.x,position.y,position.z);

        Ray ray = new Ray(position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            direction = Vector3.Reflect(direction, hit.normal);
            direction.y = 0;
            position = hit.point;
        }
        else
        {
            position += direction * 10;
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(startingPosition, position);

        DrawPredictedReflectionPattern(position, direction, reflectionsRemaining - 1);
    }
}
