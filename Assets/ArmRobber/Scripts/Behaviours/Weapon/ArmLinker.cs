﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArmLinker : MonoBehaviour
{
    public Transform linker;
    public Transform armPosition;
    public ArmWeapon armWeapon;

    private void Awake()
    {
        if (armPosition == null) return;
        if (armPosition.childCount <= 0) return;

        armWeapon = armPosition.transform.GetChild(0).gameObject.GetComponent<ArmWeapon>();
    }

    public void SetWeapon(ArmWeapon weapon)
    {
        armWeapon = weapon;
    }

    public void RemoveWeapon()
    {
        armWeapon = null;
    }
}
