﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

public class GenericBulletComponent : Bullet
{
    [SerializeField] protected GameObject impactParticle;
    [SerializeField] protected GameObject projectileParticle;
    //[SerializeField]protected GameObject[] trailParticles;

    [PunRPC]
    public override void InitializeBullet(int weaponId, int ownerId, Vector3 dir)
    {
        base.InitializeBullet(weaponId, ownerId, dir);

        ActiveProjectileParticle();

        impactParticle.transform.SetParent(null);
    }

    [PunRPC]
    public override void RPC_Initialize(Vector3 dir)
    {
        base.RPC_Initialize(dir);

        ActiveProjectileParticle();

        impactParticle.transform.SetParent(null);
    }

    [PunRPC]
    protected virtual void ActiveProjectileParticle()
    {
        projectileParticle.SetActive(true);
    }

    [PunRPC]
    protected virtual void ActiveImpactParticle(Vector3 position,Vector3 normal,bool isRpc = false)
    {
        impactParticle.transform.position = position;
        impactParticle.transform.rotation = Quaternion.FromToRotation(Vector3.up, normal);

        var impactPs = impactParticle.GetComponent<ParticleSystem>();

        impactPs.Simulate(0.0f, true, true);
        impactPs.Play();

        impactParticle.SetActive(true);

        Destroy(impactParticle,impactPs.main.duration + 0.3f);

        Debug.Log("Destroying impact particle ");
    }

    protected override void ObjectHitted(RaycastHit collision, bool destroyParticles = true)
    {
        var damageable = collision.transform.gameObject.GetComponent<DamagableComponent>();

        if (damageable != null)
            damageable.Damage(collision, DamageToInflict);

        if (destroyParticles)
            DestroyParticles(collision);
    }

    protected override void ObjectHitted(Collision collision, bool destroyParticles = true) { }

    protected virtual void DestroyParticles(RaycastHit contactPoint)
    {
        IsStopped = true;

        ActiveImpactParticle(transform.position, contactPoint.normal);

        photonView.RPC("ActiveImpactParticle", RpcTarget.Others, transform.position, contactPoint.normal, true);

        Destroy(projectileParticle, 3f);

        ParticleSystem[] trails = GetComponentsInChildren<ParticleSystem>();

        //Component at [0] is that of the parent i.e. this object (if there is any)
        for (int i = 1; i < trails.Length; i++)
        {
            ParticleSystem trail = trails[i];

            if (trail.gameObject.name.Contains("Trail"))
            {
                trail.transform.SetParent(null);
                Destroy(trail.gameObject, 2f);
            }
        }
    }

    [PunRPC]
    protected override void DestroyObject()
    {
        base.DestroyObject();
    }

    protected override void OnDestroy()
    {
        Destroy(impactParticle.gameObject,5);

        base.OnDestroy();
    }

    protected virtual IEnumerator Wait(float seconds, System.Action onComplete)
    {
        yield return new WaitForSeconds(seconds);

        onComplete?.Invoke();
    }
}
