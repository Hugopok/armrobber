﻿using DG.Tweening;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class VictoryPanel : BasePanel,IObservable
{
    [Header("Victory panel settings")]
    [SerializeField]
    private TextMeshProUGUI _victoryText;
    [SerializeField]
    private GameObject _retryButton;

    [SerializeField]
    private string _victoryMessage;

    protected override void Start()
    {
        StartListening();

        if (!PhotonNetwork.IsMasterClient)
            _retryButton.SetActive(false);
            
        base.Start();
    }

    protected override void OnDestroy()
    {
        StopListening();

        base.OnDestroy();
    }

    public void Retry()
    {   
        StartCoroutine(GameManager.Instance.Retry());

        OpenClose(false);
    }

    public void MainMenu()
    {
        GameManager.Instance.MainMenu();
    }

    protected override void OpenAnimation()
    {
        rectTransform.DOAnchorPos3D(Vector3.zero,animationTime).SetEase(easeFunction);
    }

    protected override void CloseAnimation()
    {
        rectTransform.DOAnchorPos3D(startPosition, animationTime).SetEase(Ease.OutQuad);
    }

    public void StartListening()
    {
        ObserverEventManager.AddObserver(ObserverEventNames.WIN_EVENT,this);
    }

    public void StopListening()
    {
        ObserverEventManager.RemoveObserver(ObserverEventNames.WIN_EVENT, this);
    }

    public void OnEvent(string eventName, object objectPassed)
    {
        _victoryText?.SetText(_victoryMessage.Replace("{0}",((GameObject)objectPassed).name));

        UIManager.Instance.OpenPanel<VictoryPanel>();
    }
}
