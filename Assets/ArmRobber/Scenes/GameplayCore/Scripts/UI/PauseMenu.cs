﻿
using Photon.Pun;
using UnityEngine;

public class PauseMenu : BasePanel
{
    [SerializeField]
    private GameObject _retryButton;

    protected override void Start()
    {
        base.Start();

        if (PhotonNetwork.IsMasterClient) return;

        _retryButton.gameObject.SetActive(false);
    }

    public void Retry()
    {
        StartCoroutine(GameManager.Instance.Retry());

        OpenClose(false);
    }

    public void MainMenu()
    {
        GameManager.Instance.MainMenu();
    }

    public void Continue()
    {
        OpenClose(false);

        GameManager.Instance.State = GameState.InGame;
    }

    protected override void OnClose()
    {
        base.OnClose();
    }
}
