﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharactersUIItem : MonoBehaviour
{
    public string CharacterName { get; private set; }

    [Header("Basic settings")]

    [SerializeField]
    private int _playerIndex = 0;

    [Space(10)]
    [Header("UI settings")]

    [SerializeField]
    private TextMeshProUGUI _txtArm;

    [SerializeField]
    private Image _img;

    [Space(10)]
    [Header("Animations settings")]

    [SerializeField]
    private Vector3 _shakeRotationForce;
    [SerializeField]
    private Vector3 _shakePositionForce;
    [SerializeField]
    private bool _useInvincibilityDuration;
    [SerializeField][Sirenix.OdinInspector.DisableIf("_useInvincibilityDuration",true)]
    private float _shakeDuration;
    [SerializeField]
    private Ease _easeFunction;

    private PlayerComponent _pComponent;

    private void Awake()
    {
        PlayerManager.Instance.OnGameObjectCreated.AddListener(PlayerCreated);
    }

    private void OnDestroy()
    {
        if (PlayerManager.IsInstanced())
            PlayerManager.Instance.OnGameObjectCreated.RemoveListener(PlayerCreated);

        if (_pComponent == null) return;
        if (_pComponent.ShootingComponent == null || _pComponent.ShootingComponent.DamagableComponent == null) return;

        _pComponent.ShootingComponent.DamagableComponent.OnDamage.RemoveListener(DamageEvent);
        _pComponent.ShootingComponent.DamagableComponent.OnAddHealth.RemoveListener(GrabEvent);

    }

    private void LateUpdate()
    {
        if (_pComponent == null || !gameObject.activeInHierarchy) return;

        SetArmCount(_pComponent.ShootingComponent.GetArmsCount, _pComponent.ShootingComponent.GetMaxArms);
    }

    private void PlayerCreated(GameObject go)
    {
        if (!PlayerManager.Instance.PlayersInGame.IsInBounds(_playerIndex))
        {
            gameObject.SetActive(false);
            return;
        }

        _pComponent = PlayerManager.Instance.PlayersInGame[_playerIndex];

        if (_pComponent == null)
        {
            gameObject.SetActive(false);
            return;
        }

        gameObject.SetActive(true);

        // Osserva l'evento di danno e di grab
        _pComponent.ShootingComponent.DamagableComponent.OnDamage.AddListener(DamageEvent);
        _pComponent.ShootingComponent.DamagableComponent.OnAddHealth.AddListener(GrabEvent);
        
        // prendi il nome del character per possibili futuri cambiamenti (ES: visualizzare il nome del pg)
        CharacterName = _pComponent.PlayerModel.Name;

        // Set normal sprite
        _img.sprite = _pComponent.PlayerModel.RobotModel.Images.Find(i => i.ImageType == "Normal")?.SpriteImg;

        SetArmCount(_pComponent.ShootingComponent.GetArmsCount, _pComponent.ShootingComponent.GetMaxArms);
    }

    private void Shake(float duration) 
    {
        _img.sprite = _pComponent.PlayerModel.RobotModel.Images.Find(i => i.ImageType == "Sad")?.SpriteImg;

        //transform.DOShakeRotation(duration,_shakeRotationForce,5).SetEase(_easeFunction);
        transform.DOShakePosition(duration,_shakePositionForce,5,20).SetEase(_easeFunction).OnComplete(() => 
        {
            _img.sprite = _pComponent.PlayerModel.RobotModel.Images.Find(i => i.ImageType == "Normal")?.SpriteImg;
        });
    }

    private void SetArmCount(int current,int max)
    {
        var aString = _txtArm?.text.Split(' ');

        _txtArm?.SetText($"{aString[0]} {current}/{max}");
    }

    private void GrabEvent(DamagableComponent damagableComponent)
    {
        if (!gameObject.activeInHierarchy) return;

        SetArmCount(_pComponent.ShootingComponent.GetArmsCount, _pComponent.ShootingComponent.GetMaxArms);
    }

    // mettere un evento observer anche per il danno
    private void DamageEvent(DamagableComponent damagable)
    {
        if (!gameObject.activeInHierarchy) return;

        if (_pComponent == null || _pComponent.DamagableComponent != damagable) return;

        Shake(_useInvincibilityDuration ? damagable.InvulnerableTime : _shakeDuration);

        SetArmCount(_pComponent.ShootingComponent.GetArmsCount, _pComponent.ShootingComponent.GetMaxArms);
    }
}
