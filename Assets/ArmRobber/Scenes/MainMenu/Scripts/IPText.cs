﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class IPText : MonoBehaviour
{
    [SerializeField][ReadOnly]
    private TextMeshProUGUI _text;

    private void Start()
    {
        _text = GetComponent<TextMeshProUGUI>();

        _text.SetText("Connecting to the server...");

        LobbyManager.Instance.OnConnectedToMasterClient += () =>
        {
            StartCoroutine(GetIP((ip) => 
            {
                _text.SetText($"Connected, your ip is : {ip}");
            }));
        };
    }

    IEnumerator GetIP(System.Action<string> onCompleted)
    {
        string ip = string.Empty;
        
        yield return ip = IPHelper.GetPublicIP();

        onCompleted?.Invoke(ip);
    }
}
