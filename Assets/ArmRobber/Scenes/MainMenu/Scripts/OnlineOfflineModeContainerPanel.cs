﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineOfflineModeContainerPanel : SubPanel
{
    protected override void Start()
    {
        base.Start();

        LobbyManager.Instance.OnOfflineModeActivated += StartOffline;
    }

    public void PlayOnline()
    {
        UIManager.Instance.OpenNextSubPanel<HostOrSearch>();
    }

    public void PlayLocal()
    {
        LobbyManager.Instance.OfflineMode(true);
    }

    private void StartOffline()
    {
        StartCoroutine(SManager.Instance.ChangeSceneTo(SceneName.Test_Environment, null));
    }
}
