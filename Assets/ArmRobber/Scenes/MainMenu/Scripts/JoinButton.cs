﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoinButton : AInteractableUI
{
    /// <summary>
    /// Is assigned when active method is called and set to false when disactive method is called
    /// </summary>
    public MRobberPlayer ArmRobberPlayerAssigned { get; private set; }

    [SerializeField]
    private TextMeshProUGUI _text;

    [SerializeField]
    private Color _joinedColor;

    [SerializeField]
    private string _textAfterPlayerJoined;

    private Color _startColor;

    private Image _image;

    private string _startText;

    private bool _initialized;

    protected override void Start()
    {
        if (!_initialized) Init();

        base.Start();
    }

    public void Active(MRobberPlayer armRobberPlayer)
    {
        ArmRobberPlayerAssigned = armRobberPlayer;

        gameObject.SetActive(true);

        _textAfterPlayerJoined = $"Player{armRobberPlayer.Name} <size=45%> JOINED";

        _image.color = _joinedColor;

        _text?.SetText(_textAfterPlayerJoined);
    }

    public void Disactive(MRobberPlayer armRobberPlayer)
    {
        ArmRobberPlayerAssigned = null;

        gameObject.SetActive(false);

        _image.color = _startColor;

        _text?.SetText(_startText);
    }

    private void Init()
    {
        _initialized = true;

        _image = GetComponent<Image>();

        _startColor = _image.color;

        _startText = _text?.text;
    }

    protected override void OnEnable()
    {
        if (!_initialized) Init();

        base.OnEnable();
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
       
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        
    }

    public override void OnSelect(BaseEventData eventData)
    {
        
    }

    public override void OnUpdate()
    {
        
    }
}
