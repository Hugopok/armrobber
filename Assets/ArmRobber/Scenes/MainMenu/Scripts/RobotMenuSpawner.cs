﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RobotMenuSpawner : MonoBehaviour
{
    [SerializeField]
    private List<RobotSpawnAfterJoin> _robotSpawnAfterJoins = new List<RobotSpawnAfterJoin>();

    private void Awake()
    {
        if (_robotSpawnAfterJoins.Count == 0)
            _robotSpawnAfterJoins = FindObjectsOfType<RobotSpawnAfterJoin>().ToList();

        SelectionManager.Instance.OnArmRobberPlayerJoined.AddListener(PlayerJoined);
        SelectionManager.Instance.OnArmRobberPlayerLeaved.AddListener(PlayerLeaved);
    }

    private void OnDestroy()
    {
        if (!SelectionManager.IsInstanced()) return;

        SelectionManager.Instance.OnArmRobberPlayerJoined.RemoveListener(PlayerJoined);
        SelectionManager.Instance.OnArmRobberPlayerLeaved.RemoveListener(PlayerLeaved);
    }

    public void PlayerJoined(MRobberPlayer armRobberPlayer)
    {
        foreach (var robotSpawner in _robotSpawnAfterJoins)
        {
            if (robotSpawner == null) continue;

            // Already assigned and active
            if (robotSpawner.ArmRobberPlayerAssigned != null) continue;

            robotSpawner.SpawnPlayer(armRobberPlayer);

            break;
        }
    }

    public void PlayerLeaved(MRobberPlayer armRobberPlayer)
    {
        foreach (var robotSpawner in _robotSpawnAfterJoins)
        {
            if (robotSpawner == null) continue;

            if (robotSpawner.ArmRobberPlayerAssigned == null ||
                robotSpawner.ArmRobberPlayerAssigned.Id != armRobberPlayer.Id) continue;

            robotSpawner.RemovePlayer(armRobberPlayer);

            break;
        }
    }
}
