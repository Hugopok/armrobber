﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerContainerButtonManager : MonoBehaviour
{
    [SerializeField]
    private List<JoinButton> _joinButtons = new List<JoinButton>();

    private void Awake()
    {
        if (_joinButtons.Count == 0)
            _joinButtons = FindObjectsOfType<JoinButton>().ToList();
    }

    public void PlayerJoined(MRobberPlayer armRobberPlayer)
    {
        foreach (var joinButton in _joinButtons)
        {
            if (joinButton == null) continue;

            // Already assigned and active
            if (joinButton.ArmRobberPlayerAssigned != null) continue;

            joinButton.Active(armRobberPlayer);

            break;
        }
    }

    public void PlayerLeaved(MRobberPlayer armRobberPlayer)
    {
        foreach (var joinButton in _joinButtons)
        {
            if (joinButton == null) continue;

            if (joinButton.ArmRobberPlayerAssigned == null || 
                joinButton.ArmRobberPlayerAssigned.Id != armRobberPlayer.Id) continue;

            joinButton.Disactive(armRobberPlayer);

            break;
        }
    }
}
