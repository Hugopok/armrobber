﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Photon.Pun;

public class ButtonContainerPanel : BasePanel
{
    public void Play()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.IsMessageQueueRunning = false;

            PhotonNetwork.LoadLevel(SceneName.Test_Environment.ToString());
        }
        else
        {
            PhotonNetwork.Disconnect();
            StartCoroutine(SManager.Instance.ChangeSceneTo(SceneName.Test_Environment, null));
        }
    }

    public void OnlinePlay()
    {
        OnlineOfflineModeContainerPanel onlinePanel = (OnlineOfflineModeContainerPanel)GetSubPanel<OnlineOfflineModeContainerPanel>();

        //onlinePanel?.SetState(OnlinePanelState.SearchingHost);
        
        UIManager.Instance.OpenNextSubPanel<OnlineOfflineModeContainerPanel>();
    }

    public void Options()
    {

    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }
}
