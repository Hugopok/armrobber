﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Photon.Realtime;

public class MatchItem : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _mapTitle;
    [SerializeField]
    private TextMeshProUGUI _players;
    [SerializeField]
    private bool _isStatusOk;

    private Button _btn;

    public void Init(string map,int playerCount,int maxPlayers,bool status,RoomInfo room)
    {
        _mapTitle.SetText(map);
        _players.SetText($"{playerCount}/{maxPlayers}");
        _isStatusOk = status;

        _btn = GetComponent<Button>();
        _btn.onClick.RemoveAllListeners();

        _btn.onClick.AddListener(() => 
        {
            LobbyManager.Instance.JoinRoomByName(room.Name);
        });
    }
}
