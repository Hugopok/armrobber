﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RobotSpawnAfterJoin : MonoBehaviour
{
    /// <summary>
    /// Is assigned when active method is called and set to false when disactive method is called
    /// </summary>
    public MRobberPlayer ArmRobberPlayerAssigned { get; private set; }

    [Header("Spawn Settings")]
    public Transform spawnPoint;
    
    public GameObject playerToSpawn;

    [Header("Animation settings")]
    public Transform[] path;

    public float duration;

    private GameObject robotSpawned;

    public void SpawnPlayer(MRobberPlayer robberPlayer)
    {
        ArmRobberPlayerAssigned = robberPlayer;

        robotSpawned = Instantiate(playerToSpawn,spawnPoint.position,Quaternion.identity,null);

        Vector3[] pathCoords = new Vector3[path.Length];

        for (int i = 0; i < path.Length; i++)
        {
            pathCoords[i] = path[i].position;
        }

        robotSpawned.transform.DOPath(pathCoords,duration);
    }

    public void RemovePlayer(MRobberPlayer robberPlayer)
    {
        ArmRobberPlayerAssigned = null;

        Destroy(robotSpawned);
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < path.Length; i++)
        {
            if (path[i] == null) continue;

            if (i + 1 > path.Length - 1) continue;

            Gizmos.color = Color.green;
            Gizmos.DrawLine(path[i].position,path[i+1].position);
        }   
        
    }
}
