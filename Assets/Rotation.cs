﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour
{
    public float speed = 1;
    public bool RotateX = false;
    public bool RotateY = false;
    public bool RotateZ = false;

    void FixedUpdate()
    {
        if (RotateX)
        transform.Rotate(Vector3.right * speed * Time.deltaTime, Space.World);
        if (RotateY)
            transform.Rotate(Vector3.up * speed * Time.deltaTime, Space.World);
        if (RotateZ)
            transform.Rotate(Vector3.forward * speed * Time.deltaTime, Space.World);
    }
}