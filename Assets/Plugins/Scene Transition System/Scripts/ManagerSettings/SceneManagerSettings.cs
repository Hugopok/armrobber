﻿using UnityEngine;

[CreateAssetMenu(fileName = "ManagerSettings", menuName = "KeyBiz/Scene Transition System/ManagerSettings")]
public class SceneManagerSettings : ScriptableObject
{
    public GameObject fadeEffectPrefab;
}
