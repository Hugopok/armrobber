﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Photon.Pun;

public class SManager : Singleton<SManager>
{
    public struct ObjectToPass
    {
        public object objectToPass;
        public Type objectType;
    }

    public SceneName Scene { get { return this._currentScene; } }

    public ObjectToPass objectToPass;

    public Action<SceneName> OnSceneChangedParamFadeOutCompleted;
    public Action<SceneName> OnSceneChangedParamBeforeFadeOut;
    public Action<SceneName> OnBeforeSceneChangeParam;
    public Action<SceneName> OnSceneChanged;

    public Action<SceneName> OnSceneUnloaded;
    public Action<SceneName> OnSceneLoaded;


    [SerializeField]
    protected SceneManagerSettings effectFadeSettings;
    [SerializeField]
    protected FadeBase currentFade;
    [Sirenix.OdinInspector.ReadOnly][SerializeField]
    private SceneName _currentScene;

    private void Awake()
    {
        _instance = this;

        DontDestroyOnLoad(this.gameObject);

        try
        {
            _currentScene = (SceneName)Enum.Parse(typeof(SceneName), SceneManager.GetActiveScene().name);
        }
        catch (Exception e)
        {
            return;
        }

        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
        SceneManager.sceneLoaded += this.OnOpenScene;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        OnSceneLoaded?.Invoke((SceneName)Enum.Parse(typeof(SceneName), arg0.name));
    }

    private void SceneManager_sceneUnloaded(Scene arg0)
    {
        OnSceneUnloaded?.Invoke((SceneName)Enum.Parse(typeof(SceneName), arg0.name));
    }

    public IEnumerator ChangeSceneTo(SceneName scene, Action<float> onProgress)
    {
        var fade = this.InstantiateFadeEffect();

        fade.FadeIn(fade.FadeSpeed,fade.Ease);

        this._currentScene = scene;

        if (fade != null)
            yield return new WaitForSecondsRealtime(fade.FadeSpeed);

        OnBeforeSceneChangeParam?.Invoke(scene);

        var sceneLoaded = SceneManager.LoadSceneAsync(scene.ToString());

        while (!sceneLoaded.isDone)
        {
            onProgress?.Invoke(Mathf.Clamp01(sceneLoaded.progress / .9f));
            yield return null;
        }
    }

    public void ChangeSceneTo(SceneName scene)
    {
        var fade = this.InstantiateFadeEffect();

        this._currentScene = scene;

        SceneManager.LoadScene(scene.ToString());
    }

    private void OnOpenScene(Scene sceneLoaded, LoadSceneMode mode)
    {
        if (currentFade != null)
        {
            currentFade.transform.SetAsFirstSibling();

            OnSceneChangedParamBeforeFadeOut?.Invoke(_currentScene);

            currentFade.OnFadeOutCompleted += () =>
            {
                OnSceneChangedParamFadeOutCompleted?.Invoke(_currentScene);
            };

            currentFade.FadeOut(this.currentFade.FadeSpeed, this.currentFade.Ease);

            currentFade = null;

            objectToPass.objectToPass = null;
            objectToPass.objectType = null;

            return;
        }
        else
        {
            _currentScene = (SceneName)Enum.Parse(typeof(SceneName), SceneManager.GetActiveScene().name);

            OnSceneChangedParamBeforeFadeOut?.Invoke(_currentScene);
        
            objectToPass.objectToPass = null;
            objectToPass.objectType = null;
            OnBeforeSceneChangeParam?.Invoke(_currentScene);
        }
        
    }

    private FadeBase InstantiateFadeEffect()
    {
        if (this.effectFadeSettings == null)
            this.effectFadeSettings = Resources.Load<SceneManagerSettings>("Settings/ManagerSettings");

        var fadeGO = GameObject.Instantiate(this.effectFadeSettings.fadeEffectPrefab);
        var fade = fadeGO.GetComponentInChildren<FadeBase>();

        if (fade == null)
            fade = fadeGO.GetComponent<FadeBase>();

        this.currentFade = fade;

        return fade;
    }

}
