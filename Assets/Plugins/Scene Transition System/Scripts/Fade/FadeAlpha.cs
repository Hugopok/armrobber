﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FadeAlpha : FadeBase
{
    [SerializeField]
    protected Color endColor;

    [SerializeField]
    protected Image imageToFade;

    [SerializeField]
    protected Color startColor;

    protected override void Awake()
    {
        base.Awake();

        if (this.imageToFade != null)
            this.startColor = this.imageToFade.color;
    }

    public override void FadeIn(float duration, Ease ease)
    {
        if (imageToFade == null) return;

        //if (DOTween.IsTweening(imageToFade)) return;

        imageToFade.DOColor(endColor, duration).OnComplete(() => 
        {
            OnFadeInCompleted?.Invoke(); 
        }).SetEase(ease);
    }

    public override void FadeOut(float duration, Ease ease)
    {
        if (imageToFade == null) return;

        imageToFade.DOColor(startColor, duration).OnComplete(() => 
        {
            isDone = true; 
            OnFadeOutCompleted?.Invoke();
            Destroy(this.gameObject);

        }).SetEase(ease);
    }
}
