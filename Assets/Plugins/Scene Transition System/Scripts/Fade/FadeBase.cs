﻿using UnityEngine;
using DG.Tweening;

public abstract class FadeBase : MonoBehaviour
{
    public bool IsDone => isDone;
    public float FadeSpeed { get { return this.fadeSpeed; } set { fadeSpeed = value; } }
    public Ease Ease { get { return this.ease; } }

    public System.Action OnFadeInCompleted;
    public System.Action OnFadeOutCompleted;

    [SerializeField]
    protected float fadeSpeed;
    [SerializeField]
    protected Ease ease;

    protected bool isDone;

    protected virtual void Awake()
    {
        var fade = FindObjectOfType<FadeBase>();

        if (fade != null && fade.GetInstanceID() != GetInstanceID())
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject); 
    }
    
    public abstract void FadeIn(float duration, Ease ease);

    public abstract void FadeOut(float duration, Ease ease);
}
