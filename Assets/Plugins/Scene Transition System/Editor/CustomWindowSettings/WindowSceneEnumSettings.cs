﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if WINDOW_HELPER
using WindowHelper;
#endif
[CreateAssetMenu(fileName = "WindowSceneSettings", menuName = "KeyBiz/Scene Transition System/WindowSceneSettings")]
public class WindowSceneEnumSettings : WindowSettings
{
    public string csFilePath;
    public List<string> sceneFolders = new List<string>();

    [Space(10)]
    [Header("Custom icons")]
    public Sprite RefreshSprite;
}
