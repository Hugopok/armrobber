﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeButtonScene : MonoBehaviour
{
    public SceneName scene;

    public void ChangeScene()
    {
        StartCoroutine(LoadScene(this.scene));
    }

    IEnumerator LoadScene(SceneName scene)
    {
        yield return SManager.Instance.ChangeSceneTo(scene, (float p) => { Debug.Log("Progress: " + p); });
    }
}
