﻿#if WINDOW_HELPER
using UnityEngine;

namespace WindowHelper
{
    [CreateAssetMenu(fileName = "WindowSettings", menuName = "KeyBiz/WindowHelper/WindowSettings")]
    public class WindowSettings : ScriptableObject
    {
        public string title;
        public bool utility;
        public Texture logo;

        public Vector2 minSize;
        public Vector2 maxSize;
    }
}
#endif