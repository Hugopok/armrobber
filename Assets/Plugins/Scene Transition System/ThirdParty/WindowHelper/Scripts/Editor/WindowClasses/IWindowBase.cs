﻿using System;

namespace WindowHelper
{
    public interface IWindowBase
    {
        void OnOpenWindow<T>(Type settingsType) where T : WindowSettings;

        void OnGUi();

        void OnWindowDestroy();

        void OnWindowDisable();

        void OnWindowEnable();

        void OnWindowUpdate();

        void OnProjectChanged();

        void OnEditorUpdate();
    }
}