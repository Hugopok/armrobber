﻿using UnityEngine;
using UnityEditor;
using System.Linq;

public class Initializer : AssetPostprocessor
{
    private const string WINDOW_HELPER_KEY = "KB_WINDOW_HELPER";
    private const string DEFINE = "WINDOW_HELPER";

    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
        string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);

        if (!importedAssets.ToList().Exists(str => str.Contains("WindowHelper"))) return;

        if(defines.Contains(DEFINE))
        {
            Debug.LogWarning(DEFINE + " already defined");
            return;
        }
        
        PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, defines + ";" + DEFINE);
        
        Debug.Log("DEFINE ADDED : " + DEFINE);
    }
}
