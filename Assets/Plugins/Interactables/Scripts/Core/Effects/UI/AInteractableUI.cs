﻿using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class AInteractableUI : AInteractable,
                                        ISelectHandler,
                                        IDeselectHandler,
                                        IPointerUpHandler,
                                        IPointerClickHandler, 
                                        IPointerDownHandler, 
                                        IPointerEnterHandler, 
                                        IPointerExitHandler
{
    [SerializeField]
    protected float duration;

    [SerializeField]
    protected bool makeFrameIndependent = true;

    public UnityEvent OnSelected = new UnityEvent();
    public UnityEvent OnDeselected = new UnityEvent();

    protected override void Start()
    {
        base.Start();
    }

    protected virtual void Awake() { }

    public abstract void OnPointerClick(PointerEventData eventData);

    public abstract void OnPointerDown(PointerEventData eventData);

    public abstract void OnPointerEnter(PointerEventData eventData);

    public abstract void OnPointerExit(PointerEventData eventData);

    public abstract void OnPointerUp(PointerEventData eventData);

    public abstract void OnSelect(BaseEventData eventData);

    public abstract void OnDeselect(BaseEventData eventData);
}
